import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { ThemeProvider } from "@mui/material/styles";
import { BrowserRouter } from "react-router-dom";
import { createTheme } from "@mui/material/styles";
import AppProvider from "./states/context";
import "./index.css";

const theme = createTheme({
  palette:{
    primary:{
      main:"#1976D2",
      light:"#42a5f5",
      dark:"#1565c0",
    },
    secondary:{
      main:"#616161",
      light:"#90A1AD",
    },
    success:{
        main:"#4caf50",
        light:"#2e7d32",
        dark:"#1b5e20",
        contrastText:"#FFFFFF"
    },
    error:{
      main:"#d32f2f",
      light:"#ef5350",
      dark:"#c62828"
    },
  
  }
});

ReactDOM.render(
  <BrowserRouter>
    <AppProvider>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </AppProvider>
  </BrowserRouter>,
  document.getElementById("root")
);
