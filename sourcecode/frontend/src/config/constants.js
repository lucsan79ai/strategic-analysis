// const
export const entityType = {
    PRODUCT: 'Product',
    BOM: 'BOM',
    CHANGE: 'Change',
    WHERE_USED: 'Where used'
}


export const originType = {
    QUERY: 'Query',
    TABLE: 'Table'
}


export const dataSourceType = {
    DATABASE: 'DATABASE',
    CAD: 'CAD',
    FILE: 'FILE',
    PLM: 'PLM'
}


export const status = {
    PUBLIC: 'PUBLIC',
    PRIVATE: 'PRIVATE'
}