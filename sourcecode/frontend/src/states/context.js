import { createContext, useReducer } from "react";
import { dataSourceReducer, entityReducer } from "./reducer";

export const AuthContext = createContext();

const initialStateDataSource = {
    dataSourcesArray:[],
    currentDataSource: {
        id: -1,
        connectionType: "",
        sourceName: "",
        sourceDescription: "",
        baseUrl: "",
        host: "",
        port: "",
        databaseName: "",
        username: "",
        password: "",
        status: "",
        dataSourceType:"",
        currentEntitiesArray:[]
    }
}

const initialStateEntity = {
  currentEntity: {
    id: -1,
    dataSourceID: -1,
    label: "",
    description: "",
    domain: "",
    businessObject: "",
    entityType: "",
    originType: "",
    originName: "",
    originDirectQuery:"",
    // originNavigationUrl: null,    
    currentTagsArray: [],
    currentSchemaArray: [],
    dataSourceType:""
  }

  /*   
  emptySchemaItem: {
      id: -1,
      dataSourceEntityID: -1,
      fieldType: "",
      fieldName: "",
      label: "",
      description: "",
      calculated: false,
      meausure: false,
      hidden: false,
      order: 22,
      preview: true,
      previewOrder: 2,
      fieldKey: "",
  } 
  */
}



const AppProvider = (props) => {

  const [dataSourceContext, dataSourceDispatch] = useReducer(dataSourceReducer, initialStateDataSource)
  const [entityContext, entityDispatch] = useReducer(entityReducer, initialStateEntity)
  
  //Bisogna creare gli stati da condividere con tutti i componenti e passarli in
  //value dentro AuthContext.Provider




  return <AuthContext.Provider value={{
    entityContext,
    dataSourceContext,
    dataSourceDispatch,
    entityDispatch
  }}>{props.children}</AuthContext.Provider>;
};

export default AppProvider;
