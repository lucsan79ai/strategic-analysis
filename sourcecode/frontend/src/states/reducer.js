import { 
  CURRENT_DATA_SOURCE_ARRAY, CURRENT_DATA_SOURCE, CURRENT_DATA_SOURCE_INIT, CURRENT_DATA_SOURCE_ENTITY_ARRAY,  
  CURRENT_DATA_SOURCE_UPDATE_DATASOURCE_TYPE, CURRENT_DATA_SOURCE_STATUS,
  CURRENT_ENTITY, CURRENT_ENTITY_TAG_ARRAY, CURRENT_ENTITY_SCHEMA_ARRAY, CURRENT_ENTITY_INIT, CURRENT_ENTITY_DATA_SOURCE_ID, 
  CURRENT_ENTITY_DATA_SOURCE_TYPE, CURRENT_ENTITY_DOMAIN, CURRENT_ENTITY_BUSINESS_OBJECT
 } from "./actions";


 import { dataSourceType, status } from '../config/constants'


export const dataSourceReducer = (state, action) => {
  
  switch (action.type) {
    
    case CURRENT_DATA_SOURCE_ARRAY:
      return { 
        ...state, 
        dataSourcesArray: action.payload 
      };
    

    case CURRENT_DATA_SOURCE:
      return { 
        ...state,
        currentDataSource: action.payload 
      };


    case CURRENT_DATA_SOURCE_STATUS:
      return { 
        currentDataSource: {
          ...state.currentDataSource,
          status: action.payload 
        }
      };
      

    case CURRENT_DATA_SOURCE_INIT:
      return { 
        ...state, 
        currentDataSource: {
          // id: -1,
          dataSourceType: dataSourceType.DATABASE,
          connectionType: "",
          sourceName: "",
          sourceDescription: "",
          baseUrl: "",
          host: "",
          port: "",
          databaseName: "",
          username: "",
          password: "",
          status: status.PRIVATE,
          currentEntitiesArray: []
        } 
    }
      
    case CURRENT_DATA_SOURCE_ENTITY_ARRAY:
      return { 
        currentDataSource: {
          ...state.currentDataSource,
          currentEntitiesArray: action.payload 
        }
      }  

    case CURRENT_DATA_SOURCE_UPDATE_DATASOURCE_TYPE:
      return { 
        currentDataSource: {
          ...state.currentDataSource,
          dataSourceType: action.payload 
        }
      }  

    default:
      break;
  }

};




export const entityReducer = (state, action) => {
  switch (action.type) {
    

    // OK
    case CURRENT_ENTITY:
      return { 
        currentEntity: {
          ...state.currentEntity,
          ...action.payload
        }
      };


    case CURRENT_ENTITY_DATA_SOURCE_ID: 
      console.log('Reducer CURRENT_ENTITY_DATA_SOURCE_ID called: ' + action.payload)
      return {
        currentEntity: {
          ...state.currentEntity,
          dataSourceID: action.payload
        }
      }


    case CURRENT_ENTITY_DOMAIN: 
      console.log('Reducer CURRENT_ENTITY_DOMAIN called: ' + action.payload)
      return {
        currentEntity: {
          ...state.currentEntity,
          domain: action.payload
        }
      }


    // OK
    case CURRENT_ENTITY_BUSINESS_OBJECT: 
      console.log('Reducer CURRENT_ENTITY_BUSINESS_OBJECT called: ' + action.payload)
      return {
        currentEntity: {
          ...state.currentEntity,
          businessObject: action.payload
        }
      }


    // OK
    case CURRENT_ENTITY_SCHEMA_ARRAY:
      return { 
        currentEntity: {
          ...state.currentEntity,
          currentSchemaArray: action.payload          
        }
    };


    case CURRENT_ENTITY_DATA_SOURCE_TYPE:
      console.log('Reducer CURRENT_ENTITY_DATA_SOURCE_TYPE called: ' + action.payload)
      return{
        currentEntity: {
          ...state.currentEntity,
          dataSourceType: action.payload          
        }
      }
      

    case CURRENT_ENTITY_TAG_ARRAY:
      return { 
        ...state,
        ...state.currentSchemaArray, 
        currentTagsArray: action.payload
      };  
    
 
    case CURRENT_ENTITY_INIT: 
      return { 
        currentEntity: {
          // id: -1,
          dataSourceID: -1,
          label: "",
          description: "",
          entityType: "",
          originType: "",
          originName: "",
          domain: "",
          businessObject: "",
          originDirectQuery:"",
          // originNavigationUrl: null,    
          currentTagsArray: [],
          currentSchemaArray: [],
          dataSourceType:""
        }
        
        /*   
        ...state, 
        ...state.currentEntity,
        currentEntity: {
          label: "",
          description: "",
          entityType: "",
          originType: "",
          originName: "",
          originDirectQuery:"",
        }, 
        currentTagsArray:[],
        currentSchemaArray:[] 
        */
      };

    default:
      break;
  }
};

