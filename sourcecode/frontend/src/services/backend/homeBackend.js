
export const getStatsSummary = async () => {
    console.log('getStatsCard called')
    return new Promise((resolve, reject) => resolve({
      error: false,
      data: statsData
    }));
};



export const getLastSearchesSummary = async () => {
    console.log('getLastSearches called')
    return new Promise((resolve, reject) => resolve({
      error: false,
      data: lastSearches
    }));
};


export const getSuggestedTopicsSummary = async () => {
    console.log('getSuggestedTopics called')
    return new Promise((resolve, reject) => resolve({
      error: false,
      data: suggestedTopics
    }));
};


export const search = async (queryType, queryText, topics) => {
    console.log('search called')
    return new Promise((resolve, reject) => resolve({
      error: false,
      data: searchData
    }));
};
  