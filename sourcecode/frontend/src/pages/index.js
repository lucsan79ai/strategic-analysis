import { makeStyles } from "@mui/styles";

export default makeStyles(()=>({

    container:{
        marginTop:'15px',
    },
    
    backButton: {
        backgroundColor: "#616161 !important",
        marginRight: "1rem !important",
        borderRadius:"3rem !important",
        minWidth:"0 !important"
      },

}))