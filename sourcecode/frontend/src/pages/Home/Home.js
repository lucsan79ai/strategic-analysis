import { TextField, Button,Grid,Grow } from "@mui/material";
import React from "react";
import CardHome from "../../components/commons/CardHome/CardHome";
import useStyles from "./index";
import logo from "../../assets/images/logo.jpg"

const Home = () => {
  const classes = useStyles();

  return (
    <Grow in>
    <div className={classes.container}>
      <img src={logo} alt="logo" className={classes.imgFitContainer}/>
      <form className={classes.inputContainer}>
        <TextField></TextField>
        <Button sx={{ marginLeft: "2rem" }} variant="outlined">
          Search
        </Button>
      </form>

      <div className={classes.cardContainer}>
        <Grid container padding={1} spacing={2} display="flex">
          <Grid item xs={12} md={4}>
            <CardHome
              title="Stats"
              secondTitle={"Dark card title"}
              content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus,
            nulla ut commodo sagittis."
              colorCard="#424242"
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <CardHome
              title="Last searches"
              secondTitle={"Primary card title"}
              content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus,
                nulla ut commodo sagittis."
              colorCard="#0D6EFD"
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <CardHome
              title="Sugegsted  topics"
              secondTitle={"Primary card title"}
              content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus,
                nulla ut commodo sagittis."
              colorCard="orange"
            />
          </Grid>
        </Grid>
      </div>
    </div>
    </Grow>
  );
};

export default Home;
