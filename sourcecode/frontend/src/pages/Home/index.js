import { makeStyles } from "@mui/styles";

export default makeStyles(()=>({

    container:{
        display:"flex",
        flexDirection:"column",
        justifyContent:"center",
        alignItems:"center"
    },
    inputContainer:{
        marginTop:"4rem",
        width:"100%",
        display:"flex",
        alignItems:"center",
        justifyContent:"center"

    },
    cardContainer:{
        marginTop:"6rem",
        maxWidth:"80%",
        display:"flex",
        alignItems:"center",
        justifyContent:"space-between"
    },
    imgFitContainer:{
        objectFit:"contain",
        marginTop:"2rem",
        maxWidth:"300px",
    
    }
}))