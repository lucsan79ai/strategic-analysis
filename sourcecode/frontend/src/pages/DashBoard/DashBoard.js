import React, { useEffect, useState } from "react";
import useStyles from "./../index";
import { Grid, Grow } from "@mui/material";
import SummaryStats from "../../components/dashboard/SummaryStats/SummaryStats";
import LineChart from "./../../components/dashboard/LineChart/LineChart";
import {getSummaryStats,getGraphData,getDataFirstCard,getDataSecondCard} from "../../services/backend/dashboardBackend";
import ProductImprovement from "../../components/dashboard/ProductImprovement/ProductImprovement";
import ProductEfficiency from "../../components/dashboard/ProductEfficiency/ProductEfficiency";
import Revenue from "../../components/dashboard/Revenue/Revenue";

const DashBoard = () => {
  const classes = useStyles();

  const [dataSummary, setDataSummary] = useState(null);
  const [dataGraph, setDataGraph] = useState(null);

  const [dataFirstCard, setDataFirstCard] = useState(null);
  const [dataSecondCard, setDataSecondCard] = useState(null);

  useEffect(() => {
    (async () => {
      try {
        const dataSummaryBackend = await getSummaryStats();
        const dataGraphBackend = await getGraphData();
        const dataFirstCard = await getDataFirstCard();
        const dataSecondCard = await getDataSecondCard();

        setDataSummary(dataSummaryBackend);
        setDataGraph(dataGraphBackend);
        setDataFirstCard(dataFirstCard);
        setDataSecondCard(dataSecondCard);
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);

  return (
    <div className={classes.container}>
      <Grow in>
        <Grid container spacing={2}>
          {dataSummary && (
            <React.Fragment>
              <Grid item xs={12} md={4}>
                <SummaryStats
                  titleSummary={dataSummary?.totalRedundancy?.label}
                  value={dataSummary?.totalRedundancy?.value}
                  percentage={dataSummary?.totalRedundancy?.percentage}
                />
              </Grid>
              <Grid item md={4} xs={12}>
                <SummaryStats
                  titleSummary={dataSummary?.geometryFix?.label}
                  value={dataSummary?.geometryFix?.value}
                  percentage={dataSummary?.geometryFix?.percentage}
                />
              </Grid>
              <Grid item md={4} xs={12}>
                <SummaryStats
                  titleSummary={dataSummary?.engineeringChanges?.label}
                  value={dataSummary?.engineeringChanges?.value}
                  percentage={dataSummary?.engineeringChanges?.percentage}
                />
              </Grid>
            </React.Fragment>
          )}

          {dataGraph && (
            <Grid item md={12} xs={12}>
              <LineChart dataGraph={dataGraph} />
            </Grid>
          )}
          {dataFirstCard && (
            <Grid item xs={12} md={4}>
              <ProductImprovement dataFirstCard={dataFirstCard} />
            </Grid>
          )}

          {dataSecondCard && (
            <Grid item xs={12} md={4}>
              <ProductEfficiency dataSecondCard={dataSecondCard} />
            </Grid>
          )}

          <Grid item xs={12} md={4}>
            <Revenue />
          </Grid>
        </Grid>
      </Grow>
    </div>
  );
};

export default DashBoard;
