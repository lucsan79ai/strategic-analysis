import React,{ useState, useCallback, useEffect } from 'react';
import useStyles from "../index.js";
import {useNavigate} from "react-router-dom"

import { useContext } from "react";
import { AuthContext } from "../../states/context.js";

import { CURRENT_ENTITY_INIT } from "../../states/actions" 

import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import SendIcon from '@mui/icons-material/Send';


import NewEntityStep3 from '../../components/entities/NewEntity/NewEntityStep3'
import NewEntityStep4 from '../../components/entities/NewEntity/NewEntityStep4'

import NewEntityPlmCadStep1 from '../../components/entities/NewEntity/NewEntityPlmCadStep1'
import NewEntityPlmCadStep2 from '../../components/entities/NewEntity/NewEntityPlmCadStep2'




import {
    Stepper, Step, StepLabel,
    Typography,
    Box,
    Button
  } from "@mui/material";
import { insertNewEntity } from '../../services/backend/dataSourcesBackend.js';

import { entityType, originType } from '../../config/constants'

const steps = ['Select Entity type and Origin type', 'General Information', "Schema Optimizazion", "Manage Topics"];


const NewEditEntityPlmCad = ({isNew}) => {



    const classes = useStyles();

    const ctx = useContext(AuthContext)
    const { dataSourceDispatch, dataSourceContext, entityDispatch, entityContext } = ctx
  
    const [activeStep, setActiveStep] = useState(0);

    const [canGoOn, setCanGoOn] = useState(true);

    const navigate = useNavigate()

    const handleNext = () => {
        console.log('handleNext --> activeStep --> ', activeStep)
        // console.log('entityType: ', currentEntityType)
        if (activeStep === 0) 
            setCanGoOn(false);
        if (entityContext.currentEntity.businessObject != "")
            setCanGoOn(true);
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };


    const handleBack = () => {
        if (activeStep === 1) 
            setCanGoOn(true);
        if (activeStep === 2)
            setCanGoOn(true);

        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const gotoDataSourceDetails = useCallback( (e)=>{
        // TODO deve andare al dettaglio del datasource
        entityDispatch({type: CURRENT_ENTITY_INIT})
        navigate(`/dataSources/${entityContext.currentEntity.dataSourceID}` )
    },[navigate])


    const goToDataSources = useCallback( () => {
        entityDispatch({type: CURRENT_ENTITY_INIT})
        navigate("/dataSources")
    },[navigate])


    const saveAndGoToDataSources = async () => {
        await insertNewEntity(entityContext.currentEntity);
        navigate("/dataSources")
    }

    const renderTitle = () => {
        if (isNew)
            return (<>Add an Entity - Datasource type: CAD / PLM</>)
        else 
            return (<>Edit the Entity - Datasource type: CAD / PLM</>)
    }

    return (

        <div className={classes.container} style={{padding:"30px"}}>
            <Box
                display={"flex"}
                alignItems="center"
                marginTop={3}
            >
                <Button  className={classes.backButton}  size="small" variant="contained" onClick={ gotoDataSourceDetails }>
                    <ArrowBackIcon size="large" color="white" />
                </Button>

                <Typography
                    variant="h5"
                    display={"flex"}
                    fontWeight={500}
                    color="primary"
                    alignItems={"center"}
                    >
                {renderTitle()}
                </Typography>
            </Box>
       

            <div style={{marginTop:"40px"}}>

            <Box sx={{ width: '100%' }} >
                <Stepper activeStep={activeStep}>
                    {steps.map((label, index) => {
                        const stepProps = {};
                        const labelProps = {};
                        
                        return (
                            <Step key={label} {...stepProps}>
                            <StepLabel {...labelProps}>{label}</StepLabel>
                            </Step>
                        );
                    })}
                </Stepper>
                {activeStep === steps.length ? (
                   <div style={{marginTop: "40px"}}>
                   <Typography sx={{ mt: 2, mb: 1, textAlign: 'center' }}>
                        All right! You can save the new entity or skip the changes
                        </Typography>
                    <Box sx={{ textAlign: 'center', marginTop: "40px" }} >
                        <Button variant="contained" onClick={saveAndGoToDataSources}>Save and Go to dataSources list</Button>
                    </Box> 
                    <Box sx={{ textAlign: 'center', marginTop: "40px" }} >
                        <Button onClick={goToDataSources}>Go to dataSources list without saving</Button>
                    </Box> 
                    </div>
                ) : (
                    <div style={{marginTop:"10px", padding:"30px"}}>



                        <NewEntityPlmCadStep1 visible = {activeStep == 0} setCanGoOn = {setCanGoOn}/>

                        <NewEntityPlmCadStep2 visible = {activeStep == 1} setCanGoOn = {setCanGoOn}/>

                        <NewEntityStep3 visible = {activeStep == 2}/>

                        <NewEntityStep4 visible = {activeStep == 3}/>

                        <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
                             {(activeStep !== 0) && <Button
                                color="inherit"
                                onClick={handleBack}
                                sx={{ mr: 1 }}
                                
                                variant="outlined"
                                >
                                Back
                            </Button>}
                            <Box sx={{ flex: '1 1 auto' }} />

                            <Button onClick={handleNext} variant="contained" disabled={!canGoOn} endIcon={<SendIcon />}>
                            {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                            </Button>
                        </Box>
                    </div>
                )}
            </Box>
            </div>
     
        </div>
    
    
    )



}


export default NewEditEntityPlmCad