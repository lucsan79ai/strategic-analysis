import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getDataEntities } from "../../services/backend/dataSourcesBackend";
import InfoEntityDbTable from "../../components/entityDetail/InfoEntity/InfoEntityDbTable";
import InfoEntityDbQuery from "../../components/entityDetail/InfoEntity/InfoEntityDbQuery";
import InfoEntityCadPlm from "../../components/entityDetail/InfoEntity/InfoEntityCadPlm";
import TagComponent from "../../components/entityDetail/TagComponent/TagComponent";
import HeaderPage from "../../components/commons/HeaderPage/HeaderPage";
import SearchAddComponent from "../../components/commons/SearchAddComponent/SearchAddComponent";
import TableEntitySchema from "../../components/entityDetail/TableEntitySchema/TableEntitySchema";
import useStyles from "../index";

import { CURRENT_ENTITY, CURRENT_ENTITY_SCHEMA_ARRAY } from '../../states/actions'


import { useContext } from "react";
import { AuthContext } from "../../states/context";
import { dataSourceType, originType }  from '../../config/constants'


const EntityDetail = () => {
  const ctx = useContext(AuthContext)
  const { dataSourceDispatch, dataSourceContext, entityDispatch, entityContext } = ctx

  
  const params = useParams();
  const { id } = params;
  const classes = useStyles();
  const [entityData, setEntityData] = useState(null);
  const [generalInfo, setGeneralInfo] = useState(null);



  useEffect(() => {
    (async () => {
      const data = await getDataEntities(Number(id));
      entityDispatch({type: CURRENT_ENTITY_SCHEMA_ARRAY, payload: data.dataEntitySchema})
      setEntityData(data.dataEntitySchema);
      setGeneralInfo({
        label: data.label || "non specificato",
        description: data.description || "non specificato",
        domain: data.domain || "non specificato",
        businessObject: data.businessObject || "non specificato",
        entityType: data.entityType || "non specificato",
        originType: data.originType || "non specificato",
        originName: data.originName || "non specificato",
        originDirectQuery: data.originDirectQuery || "non specificato",
        dataSourceType: data.dataSourceType || "non specificato"
      })
    })();
  }, [id]);


  function renderEntityInfo() {
    if (generalInfo.dataSourceType == dataSourceType.DATABASE) {
        if (generalInfo.originType == originType.QUERY) {
          console.log('renderEntityInfo 001')
          return (
            <InfoEntityDbQuery generalInfo={generalInfo} />
          )
        }
        else {
          console.log('renderEntityInfo 002')
          return (
            <InfoEntityDbTable generalInfo={generalInfo} />
          )
        }
    } 
    else if (generalInfo.dataSourceType == dataSourceType.CAD || generalInfo.dataSourceType == dataSourceType.PLM) {
      console.log('renderEntityInfo 003')
      return (
        <InfoEntityCadPlm generalInfo={generalInfo} />
      )
    }
    else {
      console.log('renderEntityInfo 004')

      return (<></>)
    }
  }
  


  return (
    <div className={classes.container} style={{ padding: "0 3rem" }}>
      { generalInfo && renderEntityInfo() }
      <hr />
     
      <HeaderPage title="Entity Schema" />
      <SearchAddComponent deleteButton={true} />
      {entityData && (
        <TableEntitySchema entityData={entityData} colorHeader="#90A1AD" />
      )}
            <hr />

       { entityData &&  <TagComponent entityData={entityData} />}
       <hr />
       <hr />
    </div>
  );
};

export default EntityDetail;
