import React,{ useState, useCallback } from 'react';
import useStyles from "../index.js";
import {useNavigate} from "react-router-dom"
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

import Step1 from '../../components/dataSources/NewDataSource/Step1'
import Step2_database from '../../components/dataSources/NewDataSource/Step2_database'
import Step2_cad from '../../components/dataSources/NewDataSource/Step2_cad'
import Step2_file from '../../components/dataSources/NewDataSource/Step2_file'
import Step2_plm from '../../components/dataSources/NewDataSource/Step2_plm'
import Step3 from '../../components/dataSources/NewDataSource/Step3'

import SendIcon from '@mui/icons-material/Send';

import { useContext } from "react";
import { AuthContext } from "../../states/context.js";
import { CURRENT_DATA_SOURCE_INIT } from "../../states/actions" 

import { insertNewDataSource } from '../../services/backend/dataSourcesBackend'

import {
    Stepper, Step, StepLabel,
    Typography,
    Box,
    Button
  } from "@mui/material";

  import { loadType } from '../../config/constants'


const steps = ['Select the type', 'Fill the data and test'];





function renderStep2(activeStep, currentLoadType, setCanGoOn) {


    console.log(activeStep,currentLoadType  )
    if (activeStep == 1) {
        console.log()
        switch (currentLoadType) {
            case loadType.DATABASE:
                console.log('loadType.DATABASE');
                return <Step2_database setCanGoOn={setCanGoOn}/>

            case loadType.CAD:
                console.log('loadType.CAD');
                return <Step2_cad setCanGoOn={setCanGoOn}/>

            case loadType.FILE:
                console.log('loadType.FILE')
                return <Step2_file setCanGoOn={setCanGoOn}/>

            case loadType.PLM:
                console.log('loadType.PLM')
                return <Step2_plm setCanGoOn={setCanGoOn}/>
        } 
    }
}




const NewDataSource = () => {
    const classes = useStyles();

    const [activeStep, setActiveStep] = useState(0);

    const [canGoOn, setCanGoOn] = useState(true);

    const ctx=useContext(AuthContext)
    const { dataSourceDispatch, dataSourceContext } = ctx


    const [currentLoadType, setCurrentLoadType] = React.useState(loadType.DATABASE);


    const navigate = useNavigate()

    const handleCurrentLoadType = (param) => {
        setCurrentLoadType(param);
        console.log('currentLoadType: ', currentLoadType)
    }

    const handleNext = () => {
        console.log('activeStep == ' + activeStep)
        if (activeStep === 0) 
            setCanGoOn(false);
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        console.log('activeStep == ' + activeStep)
        if (activeStep === 1) 
            setCanGoOn(true);
        if (activeStep === 2)
            setCanGoOn(false);

        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };


 
    const goToDataSources = useCallback( () => {
        dataSourceDispatch({type: CURRENT_DATA_SOURCE_INIT})
        navigate("/dataSources")
    },[navigate])

    const saveAndGoToDataSources = async () => {
        await insertNewDataSource(dataSourceContext.currentDataSource);
        navigate("/dataSources")
    }

    return (

        <div className={classes.container} style={{padding:"30px"}}>
             <Box
        display={"flex"}
        alignItems="center"
        marginTop={3}
      >
         <Button  className={classes.backButton}  size="small" variant="contained" onClick={goToDataSources}>
          <ArrowBackIcon size="large" color="white" />
        </Button>

            <Typography
                variant="h5"
                display={"flex"}
                fontWeight={500}
                color="primary"
                alignItems={"center"}
                >
            Connect to a new Data Source
            </Typography>
            </Box>
       

            <div style={{marginTop:"40px"}}>

            <Box sx={{ width: '100%' }} >
                <Stepper activeStep={activeStep}>
                    {steps.map((label, index) => {
                    const stepProps = {};
                    const labelProps = {};
                    
                    return (
                        <Step key={label} {...stepProps}>
                        <StepLabel {...labelProps}>{label}</StepLabel>
                        </Step>
                    );
                    })}
                </Stepper>



                {activeStep === steps.length ? (

                    <div style={{marginTop: "40px"}}>
                    <Typography sx={{ mt: 2, mb: 1, textAlign: 'center' }}>
                        Test successfully completed / something went wrong in test, but you can save datasource anyway. 
                    </Typography>
                    <Box sx={{ textAlign: 'center', marginTop: "40px" }} >
                        <Button variant="contained" onClick={saveAndGoToDataSources}>Save and Go to dataSources list</Button>
                    </Box> 
                    <Box sx={{ textAlign: 'center', marginTop: "40px" }} >
                        <Button onClick={goToDataSources}>Go to dataSources list without saving</Button>
                    </Box> 
                    </div>

                ) : (
                    <div style={{marginTop:"10px", padding:"30px"}}>

                        <Step1 
                            visible = {activeStep == 0}
                            currentLoadType = {currentLoadType} 
                            handleLoadType = { handleCurrentLoadType }
                        />

                        {renderStep2(activeStep, currentLoadType, setCanGoOn)}

                        <Step3 visible = {activeStep == 2}/>

                        <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
                             {(activeStep !== 0) && <Button
                                color="inherit"
                                onClick={handleBack}
                                sx={{ mr: 1 }}
                                
                                variant="outlined"
                                >
                                Back
                            </Button>}
                            <Box sx={{ flex: '1 1 auto' }} />
                            

                            <Button onClick={handleNext} variant="contained" disabled={!canGoOn} endIcon={<SendIcon />}>
                            {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                            </Button>
                        </Box>
                    </div>
                )}
            </Box>
            </div>

            

        
        </div>
        
    )

}

export default NewDataSource;
