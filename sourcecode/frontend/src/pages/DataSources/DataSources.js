import React, {  useEffect } from "react";
import useStyles from "./../index.js";
import TableDataSource from "../../components/dataSources/TableDataSources/TableDataSource";
import HeaderPage from "../../components/commons/HeaderPage/HeaderPage";
import { getDataSources } from "../../services/backend/dataSourcesBackend.js";
import { useContext } from "react";
import { AuthContext } from "../../states/context.js";
import {CURRENT_DATA_SOURCE_ARRAY} from "../../states/actions"


const DataSources = () => {
  const classes = useStyles();
  const ctx=useContext(AuthContext)
  const {dataSourceDispatch,dataSourceContext} =ctx

  useEffect(() => {
    (async () => {
      const data = await getDataSources();
      dataSourceDispatch({type:CURRENT_DATA_SOURCE_ARRAY,payload:data})
    })();
  }, [dataSourceDispatch]);

  
  return (
    <div className={classes.container} style={{ padding: "0 3rem" }}>
      <HeaderPage  title="Data Sources list" />
      {dataSourceContext.dataSourcesArray && <TableDataSource  dataSources={dataSourceContext.dataSourcesArray} />}

    </div>
  );
};

export default DataSources;
