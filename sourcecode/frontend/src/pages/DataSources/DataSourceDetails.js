import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import useStyles from "../index";
import HeaderPage from "../../components/commons/HeaderPage/HeaderPage";
import InfoDataSourceDb from "../../components/dataSourcesDetails/InfoDataSource/InfoDataSourceDb";
import InfoDataSourceCadPlm from '../../components/dataSourcesDetails/InfoDataSource/InfoDataSourceCadPlm'
import TableDataSourceDetails from "../../components/dataSourcesDetails/TableDataSourceDetails/TableDataSourceDetails";
import { getDataSourceDetails } from "../../services/backend/dataSourcesBackend";


import { useContext } from "react";
import { AuthContext } from "../../states/context.js";
import { CURRENT_DATA_SOURCE, CURRENT_ENTITY_DATA_SOURCE_ID,CURRENT_ENTITY_DATA_SOURCE_TYPE,CURRENT_ENTITY_INIT } from "../../states/actions" 

import { dataSourceType } from "../../config/constants"

const DataSourceDetails = () => {
  
  const params = useParams();
  const { id } = params;
  const classes = useStyles();
  
  const ctx = useContext(AuthContext)
  const { dataSourceDispatch, dataSourceContext, entityDispatch, entityContext } = ctx


  function renderInfos() {
    if (dataSourceContext.currentDataSource.dataSourceType == dataSourceType.DATABASE) 
      return (
        dataSourceContext.currentDataSource.code && 
        <InfoDataSourceDb dataSource = {dataSourceContext.currentDataSource}/>
      )
    else 
      return (
        dataSourceContext.currentDataSource.code && 
        <InfoDataSourceCadPlm dataSource = {dataSourceContext.currentDataSource}/>

      )
  }


  useEffect(() => {
    (async () => {
      // console.log('DataSourceDetails useEffect called')
      const data = await getDataSourceDetails(Number(id));
      dataSourceDispatch({type: CURRENT_DATA_SOURCE, payload: data})

      /**************************PULISCO LO STATO E AGGIORNO L'ID  ********************/
 
      //@todo non dovrebbe andare qui, ma quando si preme add oppure details relativametne ad una entity
      entityDispatch({ type: CURRENT_ENTITY_INIT })
      entityDispatch({ type: CURRENT_ENTITY_DATA_SOURCE_ID, payload: id })
      entityDispatch({ type: CURRENT_ENTITY_DATA_SOURCE_TYPE, payload: data.dataSourceType })

    })();
  }, [id]);

  
  return (
    <div className={classes.container} style={{ padding: "0 3rem" }}>
      {renderInfos()}
      <hr />
        <HeaderPage title="Entities"/>
      { dataSourceContext.currentDataSource.code && 
          <TableDataSourceDetails  
              dataSource = { dataSourceContext.currentDataSource }
              dataSourceId = { id }
          /> } 
    </div>
  );
};

export default DataSourceDetails;
