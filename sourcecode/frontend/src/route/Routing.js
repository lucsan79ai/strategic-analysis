import React from "react";
import Home from "../pages/Home/Home";
import DashBoard from "../pages/DashBoard/DashBoard";
import DataSources from "../pages/DataSources/DataSources";
import NewEditDataSource from "../pages/DataSources/NewEditDataSource"
import UseCases from "../pages/UseCases/UseCases";
import DataSourceDetails from "../pages/DataSources/DataSourceDetails";
import EntityDetail from "../pages/Entities/EntityDetail";
import NewEditEntityDatabase from '../pages/Entities/NewEditEntityDatabase'
import NewEditEntityPlmCad from '../pages/Entities/NewEditEntityPlmCad'

import { Routes, Route } from "react-router-dom";



const Routing = () => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/dashboard" element={<DashBoard/>} />
      <Route path="/dataSources" element={<DataSources />} />
      <Route path="/dataSources/new" element={<NewEditDataSource isNew={true}/>} />
      <Route path="/dataSources/edit" element={<NewEditDataSource isNew={false}/>} />
      <Route path="/dataSources/:id" element={<DataSourceDetails />} />
      <Route path="/entity/:id" element={<EntityDetail/>} />
      <Route path="/use-cases" element={<UseCases />} />
      <Route path="/entities/newDatabase/:dataSourceId" element={<NewEditEntityDatabase isNew={true}/>} />
      <Route path="/entities/editDatabase/" element={<NewEditEntityDatabase isNew={false}/>} />
      <Route path="/entities/newPlmCad/:dataSourceId" element={<NewEditEntityPlmCad isNew={true}/>} />
      <Route path="/entities/editPlmCad/" element={<NewEditEntityPlmCad isNew={false}/>} />
      

    </Routes>
  );
};

export default Routing;
