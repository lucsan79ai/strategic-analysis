import React from "react";
import Header from "./components/layout/Header/Header"
import Routing from "./route/Routing"


function App() {


  return (
    <React.Fragment>
      <div className="containerApp">
      <Header />
          <Routing/>
      </div>
    
         
    </React.Fragment>
  );
}

export default App;
