import * as React from "react";

import {
    Typography,
    FormControl,
    FormControlLabel,
    Radio,
    RadioGroup
  } from "@mui/material";


import { loadType } from '../../../config/constants'


const Step1 = (props) => {

    
    const [selection, setSelection] = React.useState(props.currentLoadType)

    const updateSelection = (event) => {
        // event.persist();
        const name = event.target.value;
        props.handleLoadType(name)
        setSelection(name);
    };

    if (props.visible == true)  
        return (
            <React.Fragment>

                <Typography variant="h6" color="primary">
                   Step One
                </Typography>
                
                <Typography variant="overline" display="block" gutterBottom>
                   Select from what source to load data 
                </Typography>

                <div style={{padding: '30px'}}>
                <FormControl>
                    <RadioGroup
                        aria-labelledby="demo-radio-buttons-group-label"
                        defaultValue="female"
                        name="radio-buttons-group"
                        onChange={updateSelection}
                    >
                        <FormControlLabel value={loadType.DATABASE} control={<Radio />}  label="Load from Database" checked={selection==loadType.DATABASE}/>
                        <FormControlLabel value={loadType.FILE} control={<Radio />} label="Load from File" checked={selection==loadType.FILE} />
                        <FormControlLabel value={loadType.PLM} control={<Radio />} label="Load from PLM/ERP System" checked={selection==loadType.PLM}/>
                        <FormControlLabel value={loadType.CAD} control={<Radio />} label="Load from CAD Application" checked={selection==loadType.CAD}/>
                        
                    </RadioGroup>
                </FormControl>
                </div>

                

            </React.Fragment>
        )
    else return (<></>)
}

export default Step1