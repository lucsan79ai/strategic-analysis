import * as React from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { 
  CURRENT_DATA_SOURCE, CURRENT_DATA_SOURCE_INIT
} from "../../../states/actions"
import { useContext } from "react";
import { AuthContext } from "../../../states/context.js";

import {
  Typography,
  Select,
  InputLabel,
  FormControl,
  MenuItem,
  Button,
  Grid,
  FormGroup,
  TextField,
} from "@mui/material";



const Step2 = (props) => {

  const ctx=useContext(AuthContext)
  const {dataSourceDispatch, dataSourceContext} = ctx

  const submitHandler = async (event, values) => {
    props.setCanGoOn(true);
    dataSourceDispatch({type: CURRENT_DATA_SOURCE, payload: formik.values })
    console.log('submit - currentDataSource', dataSourceContext.currentDataSource)
  };

  const initialValues = dataSourceContext.currentDataSource = {
    connectionType: dataSourceContext.currentDataSource.connectionType,
    sourceName: dataSourceContext.currentDataSource.sourceName,
    sourceDescription: dataSourceContext.currentDataSource.sourceDescription,
    host: dataSourceContext.currentDataSource.host,
    port: dataSourceContext.currentDataSource.port,
    // baseUrl: "",
    databaseName: dataSourceContext.currentDataSource.databaseName,
    username: dataSourceContext.currentDataSource.username,
    password: dataSourceContext.currentDataSource.password
  };

  const validationSchema = Yup.object({
    connectionType: Yup.string().required("Insert connection type"),
    sourceName: Yup.string().required("Insert source name"),
    sourceDescription: Yup.string(),
    host: Yup.string().required("Insert host"),
    port: Yup.string().required("Insert port"),
    databaseName: Yup.string().required("Insert database name"),
    username: Yup.string().required("Insert username"),
    password: Yup.string().required("Insert password"),
  });

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: validationSchema,
    onSubmit: submitHandler,
  });


  const handleFieldsUpdate = (fieldName, e) => {
    console.log('Field: ' + fieldName + ' has changed: ' + e.target.value)
    formik.handleChange(e);
    formik.setFieldValue(fieldName, e.target.value )
} 


  const handleCleanData = (e) => {


      formik.setFieldValue("connectionType", "" )
      formik.setFieldValue("sourceName", "" )
      formik.setFieldValue("sourceDescription", "" )
      formik.setFieldValue("host", "" )
      formik.setFieldValue("port", "" )
      formik.setFieldValue("baseUrl", "" )
      formik.setFieldValue("databaseName", "" )
      formik.setFieldValue("username", "" )
      formik.setFieldValue("password", "" )

      dataSourceDispatch({type: CURRENT_DATA_SOURCE_INIT})

      props.setCanGoOn(false);

  }


  return (
    <React.Fragment>
      <form onSubmit={formik.handleSubmit}>
        <Typography variant="h6" color="primary">
          Step Two
        </Typography>

        <Typography variant="overline" display="block" gutterBottom>
          Load from database
        </Typography>

        <Button onClick={handleCleanData} style={{padding: '20px'}}>Clean data</Button>

        <Grid
          container
          spacing={5}
          display="flex"
          alignItems={"center"}
          justifyContent="space-between"
        >

          <Grid item xs={12} md={6}>


            
            <FormControl fullWidth>
              <InputLabel>Connection type</InputLabel>
              <Select
                labelId="connectionType"
                id="demo-simple-select"
                type="text"
                value={formik.values.connectionType}
                label="Database type"
                onChange = { e => handleFieldsUpdate ("connectionType", e)}
                error={
                  formik.touched.connectionType &&
                  Boolean(formik.errors.connectionType)
                } 
              >
                <MenuItem value={"Microsoft_SQL"}>Microsoft SQL</MenuItem>
                <MenuItem value={"MySql"}>MySql</MenuItem>
                <MenuItem value={"PostgreSql"}>PostgreSql</MenuItem>
                <MenuItem value={"Other"}>Other</MenuItem>
              </Select>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={6}>
            <FormGroup className="formGroup">
              <TextField
                id="sourceName"
                type="text"
                label="Source Name"
                onChange = { e => handleFieldsUpdate ("sourceName", e)}
                value={formik.values.sourceName}
                error={
                  formik.touched.sourceName && Boolean(formik.errors.sourceName)
                }
              />
            </FormGroup>
          </Grid>

          <Grid item xs={12} md={6}>
            <FormGroup className="formGroup">
              <TextField
                id="sourceDescription"
                type="text"
                label="Source description"
                onChange = { e => handleFieldsUpdate ("sourceDescription", e)}
                value={formik.values.sourceDescription}
                error={
                  formik.touched.sourceDescription &&
                  Boolean(formik.errors.sourceDescription)
                }
              />
            </FormGroup>
          </Grid>

          <Grid item xs={12} md={6}>
            <FormGroup className="formGroup">
              <TextField
                id="host"
                type="text"
                label="Host"
                onChange = { e => handleFieldsUpdate ("host", e)}
                value={formik.values.host}
                error={formik.touched.host && Boolean(formik.errors.host)}
              />
            </FormGroup>
          </Grid>

          <Grid item xs={12} md={6}>
            <FormGroup className="formGroup">
              <TextField
                id="port"
                type="number"
                label="Port"
                onChange = { e => handleFieldsUpdate ("port", e)}
                value={formik.values.port}
                error={formik.touched.port && Boolean(formik.errors.port)}
              />
            </FormGroup>
          </Grid>

          <Grid item xs={12} md={6}>
            <FormGroup className="formGroup">
              <TextField
                id="databaseName"
                type="text"
                label="Database Name"
                onChange = { e => handleFieldsUpdate ("databaseName", e)}
                value={formik.values.databaseName}
                error={
                  formik.touched.databaseName &&
                  Boolean(formik.errors.databaseName)
                }
              />
            </FormGroup>
          </Grid>

          <Grid item xs={12} md={6}>
            <FormGroup className="formGroup">
              <TextField
                id="username"
                type="text"
                label="Username"
                onChange = { e => handleFieldsUpdate ("username", e)}
                value={formik.values.username}
                error={
                  formik.touched.username && Boolean(formik.errors.username)
                }
              />
            </FormGroup>
          </Grid>

          <Grid item xs={12} md={6}>
            <FormGroup className="formGroup">
              <TextField
                id="password"
                type="password"
                label="password"
                onChange = { e => handleFieldsUpdate ("password", e)}
                value={formik.values.password}
                error={
                  formik.touched.password && Boolean(formik.errors.password)
                }
              />
            </FormGroup>
          </Grid>

                

          <Grid item xs={12} md={12} >
            <div style={{margin: '10px'}}>
            <Button
              fullWidth
              type="submit"
              variant="outlined"
              color="secondary"
              size="large"
              onSubmit={formik.onSubmit}>
            
              Validate fields and test connection
            </Button>
            </div>
          </Grid>
        </Grid>
      </form>
    </React.Fragment>
  );
};

export default Step2;
