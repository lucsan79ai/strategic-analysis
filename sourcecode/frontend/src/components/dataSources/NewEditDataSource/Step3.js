import * as React from "react";
import { toast } from "react-toastify";
import * as Yup from "yup";
import { useFormik } from "formik";

import {
    Card,
    Typography,
    Divider,
    Box,
    Button,
    Grid,
    FormGroup,
    TextField,
  } from "@mui/material";

const Step3 = (props) => {
    if (props.visible == true)  
        return props.visible && (
            <React.Fragment>

            <Typography variant="h6" color="primary">
            Last step
            </Typography>

            <Typography variant="overline" display="block" gutterBottom>
            Test
            </Typography>



            </React.Fragment>
        )
    else return (<></>)
}

export default Step3