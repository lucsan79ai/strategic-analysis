import * as React from "react";

import {
    Typography,
    FormControl,
    FormControlLabel,
    Radio,
    RadioGroup
  } from "@mui/material";


import { dataSourceType } from '../../../config/constants'

import { useContext } from "react";
import { AuthContext } from "../../../states/context.js";

import { CURRENT_DATA_SOURCE_UPDATE_DATASOURCE_TYPE } from '../../../states/actions'


const Step1 = (props) => {
    

    const ctx=useContext(AuthContext)
    const { dataSourceDispatch, dataSourceContext } = ctx




    const updateSelection =  (event) => {
        dataSourceDispatch({type: CURRENT_DATA_SOURCE_UPDATE_DATASOURCE_TYPE, payload: event.target.value})
    };

    if (props.visible == true)  
        return (
            <React.Fragment>

                <Typography variant="h6" color="primary">
                   Step One
                </Typography>
                
                <Typography variant="overline" display="block" gutterBottom>
                   Select from what source to load data 
                </Typography>

                <div style={{padding: '30px'}}>
                <FormControl>
                    <RadioGroup
                        aria-labelledby="demo-radio-buttons-group-label"
                        defaultValue="female"
                        name="radio-buttons-group"
                        onChange={updateSelection}
                    >
                        <FormControlLabel value={dataSourceType.DATABASE} control={<Radio />}  label="Load from Database" checked={dataSourceContext.currentDataSource.dataSourceType ==dataSourceType.DATABASE}/>
                        <FormControlLabel value={dataSourceType.FILE} control={<Radio />} label="Load from File" checked={dataSourceContext.currentDataSource.dataSourceType==dataSourceType.FILE} />
                        <FormControlLabel value={dataSourceType.PLM} control={<Radio />} label="Load from PLM/ERP System" checked={dataSourceContext.currentDataSource.dataSourceType==dataSourceType.PLM}/>
                        <FormControlLabel value={dataSourceType.CAD} control={<Radio />} label="Load from CAD Application" checked={dataSourceContext.currentDataSource.dataSourceType==dataSourceType.CAD}/>
                        
                    </RadioGroup>
                </FormControl>
                </div>

                

            </React.Fragment>
        )
    else return (<></>)
}

export default Step1