import React, { memo,useCallback, useState } from "react";

//@mui
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  TablePagination,
  Button,
  Paper,
} from "@mui/material";

//hook
import {useNavigate} from "react-router-dom"
import useStyles from "./index.js";
import useTable,{getComparator} from "../../../customHook/useTable";
import { useTheme } from "@emotion/react";


//components
import { dataHead } from "./dataTable";
import PreviewIcon from '@mui/icons-material/Preview';
import TableHeadCostum from "../../commons/table/TableHeadCostum/TableHeadCostum";
import SearchAddComponent from "../../commons/SearchAddComponent/SearchAddComponent";

import { 
  CURRENT_DATA_SOURCE_INIT
} from "../../../states/actions"
import { useContext } from "react";
import { AuthContext } from "../../../states/context.js";





const TableDataSource = memo(({dataSources}) => {

  const { 
    //
    page,
    pages,
    setPage,
    //order
    order,
    orderBy,
    rowsForPage,

    onSort,
    onChangePage,
    onChangeRowsPerPage
  
  } = useTable({defaultOrderBy:"labl"})

  const ctx = useContext(AuthContext)
  const { dataSourceDispatch, dataSourceContext } = ctx

  const classes = useStyles();
  const theme= useTheme()
  const navigate=useNavigate()

  const [filterName,setFilterName]=useState("")


  const goToDetails=useCallback((id)=>{
    navigate(`/dataSources/${id}`)
  },[navigate])

  const addDataSourceHandler = useCallback(() => {
    dataSourceDispatch({ type: CURRENT_DATA_SOURCE_INIT })
    console.log('addDataSourceHandler - currentDataSource', dataSourceContext.currentDataSource)
    navigate("/dataSources/new");
  }, [navigate]);

  const handleFilterName = (filterName) => {
    setFilterName(filterName);
    setPage(0);
  };

   const dataFiltered = applySortFilter({
    tableData:dataSources,
    comparator: getComparator(order, orderBy),
    filterName,
  }); 

  console.log(dataFiltered)


  return (
    <React.Fragment>
      <SearchAddComponent placeHolder={"Search DataSources.."}  addButton={addDataSourceHandler}  filterName={filterName} onFilterName={handleFilterName} />
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">           
           <TableHeadCostum
             order={order}
             orderBy={orderBy}
             colorHeader={theme.palette.secondary.light}
             headLabel={dataHead}
             onSort={onSort}
           />
          <TableBody>
            {dataFiltered
              ?.slice(page * rowsForPage, page * rowsForPage + rowsForPage)
              .map((item) => (
                <TableRow key={item.id} hover>
                  <TableCell>{item.code}</TableCell>
                  <TableCell>{item.description}</TableCell>
                  <TableCell>{item.serverName}</TableCell>
                  <TableCell>{item.sourceName}</TableCell>
                  <TableCell
                    className={`${
                      item.status === "PUBLIC"
                        ? `${classes.cellSucces}`
                        : `${classes.cellWarning}`
                    } `}
                  >
                    {item.status}
                  </TableCell>
                  <TableCell>
                      <Button startIcon={<PreviewIcon/>} onClick={()=>goToDetails(item.id)} className={classes.buttonColor} variant="contained">
                        Views Details

                      </Button>

                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
        <TablePagination
          component="div"
          count={dataFiltered.length - 1}
          rowsPerPageOptions={pages}
          page={page}
          rowsPerPage={rowsForPage}
          onPageChange={onChangePage}
          onRowsPerPageChange={onChangeRowsPerPage}
        />
      </TableContainer>
    </React.Fragment>
  );
});

export default TableDataSource;


function applySortFilter({ tableData, comparator, filterName }) {
  const stabilizedThis = tableData.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });

  tableData = stabilizedThis.map((el) => el[0]);
 
  
  if (filterName) {
    tableData = tableData.filter((item) => item.description!==null && item.description.toLowerCase().indexOf(filterName.toLowerCase()) !== -1);
  } 

  return tableData;
}