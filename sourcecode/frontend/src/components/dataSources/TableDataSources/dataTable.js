export const dataHead = [
  {
    id: "code",
    label: "code",
    align: "left",
  },
  {
    id: "description",
    label: "description",
    align: "left",
  },
  {
    id: "serverName",
    label: "serverName",
    align: "left",
  },

  {
    id: "sourceName",
    label: "sourceName",
    align: "left",
  },

  {
    id: "status",
    label: "status",
    align: "left",
  },
  {
    id: "Action",
    label: "Action",
    align: "left",
  },
];
