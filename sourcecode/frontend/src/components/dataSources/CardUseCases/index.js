import { makeStyles } from "@mui/styles";

export default makeStyles(() => ({
  paperStyle: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
    padding: "5px",
    backgroundColor: "rgb(238,238,238)",
    cursor: "pointer",
    height: 70,
    boxShadow: "none",
  },
 
}));
