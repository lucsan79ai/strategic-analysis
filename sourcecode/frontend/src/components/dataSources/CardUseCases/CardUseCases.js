import React from "react";
import { Paper, Typography } from "@mui/material";
import postgreeSQL from "./../../../assets/images/postgreeSQL.png";
import useStyles from "./index";

const CardUseCases = () => {
  const classes = useStyles();

  return (
    <Paper className={classes.paperStyle}>
      <img
        src={postgreeSQL}
        width={30}
        height={30}
        alt={`${postgreeSQL.toString()}`}
      ></img>
      <Typography variant="subtitle1" fontSize={12}>
        PostgreeSQL
      </Typography>
    </Paper>
  );
};

export default CardUseCases;
