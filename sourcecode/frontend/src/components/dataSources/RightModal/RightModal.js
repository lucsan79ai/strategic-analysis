import * as React from "react";
import { useState } from "react";
import {
  Box,
  Typography,
  Modal,
  IconButton,
  Grid,
  useMediaQuery,
  Grow,
} from "@mui/material";
import useStyles from "./index";
import CancelOutlinedIcon from "@mui/icons-material/CancelOutlined";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import CardUseCases from "../CardUseCases/CardUseCases";

const RightModal = ({ setOpenModal, openModal }) => {
  const matches = useMediaQuery("(min-width:600px)");
  const [manageMenu, setManageMenu] = useState({
    loadFromDatabase: true,
    loadFromFile: true,
    loadFromPLM: true,
    loadFromCad: true,
  });

  const style = {
    position: "absolute",
    top: 0,
    right: 0,
    width: matches ? 500 : 300,
    height: "100vh",
    bgcolor: "background.paper",
    borderRadius: "10px 0px 0px 10px",
    boxShadow: 24,
    p: 4,
  };

  const handleClose = () => setOpenModal(false);
  const classes = useStyles();

  return (
    <Modal
      open={openModal}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <div className={classes.titleSection}>
          <Typography variant="h6" fontWeight={500}>
            Create a New Data Source
          </Typography>
          <IconButton color="primary" onClick={() => setOpenModal(false)}>
            <CancelOutlinedIcon />
          </IconButton>
        </div>
        <div className={classes.cardContainer}>
          <div className={classes.headerCard}>
            <IconButton
              onClick={() =>
                setManageMenu({
                  ...manageMenu,
                  loadFromDatabase: !manageMenu.loadFromDatabase,
                })
              }
            >
              <KeyboardArrowDownIcon />
            </IconButton>
            <Typography variant="h6" fontSize={18}>
              Load from Database
            </Typography>
          </div>
          {manageMenu.loadFromDatabase && (
            <Grow in>
              <Grid container spacing={1}>
                <Grid item md={4}>
                  <CardUseCases />
                </Grid>
                <Grid item md={4}>
                  <CardUseCases />
                </Grid>
                <Grid item md={4}>
                  <CardUseCases />
                </Grid>
                <Grid item md={4}>
                  <CardUseCases />
                </Grid>
              </Grid>
            </Grow>
          )}
        </div>

        <div className={classes.cardContainer}>
          <div className={classes.headerCard}>
            <IconButton
              onClick={() =>
                setManageMenu({
                  ...manageMenu,
                  loadFromFile: !manageMenu.loadFromFile,
                })
              }
            >
              <KeyboardArrowDownIcon />
            </IconButton>
            <Typography variant="h6" fontSize={18}>
              Load from Database
            </Typography>
          </div>
          {manageMenu.loadFromFile && (
            <Grow in>
              <Grid container spacing={1}>
                <Grid item md={4}>
                  <CardUseCases />
                </Grid>
                <Grid item md={4}>
                  <CardUseCases />
                </Grid>
                <Grid item md={4}>
                  <CardUseCases />
                </Grid>
                <Grid item md={4}>
                  <CardUseCases />
                </Grid>
              </Grid>
            </Grow>
          )}
        </div>
      </Box>
    </Modal>
  );
};

export default RightModal;
