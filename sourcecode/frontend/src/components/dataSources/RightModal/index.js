import { makeStyles } from "@mui/styles";


export default makeStyles(()=>({


    titleSection:{
        display:"flex",
        alignItems:"center",
        justifyContent:"space-between",
        borderBottom: "3px solid #e3dede",

    },
    cardContainer:{
        display:"flex",
        flexDirection:"column"
      },
    
      headerCard:{
          display:"flex",
          justifyContent:"start",
          alignItems:"center"
      }
 
}))