import React, { memo, useEffect, useCallback } from "react";
import {
  Grid,
  Typography,
  TextField,
  FormGroup,
  Button,
  Box,
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from "@mui/material";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import EditIcon from "@mui/icons-material/Edit";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {updateEntity,deleteEntity,} from "../../../services/backend/dataSourcesBackend";
import { useNavigate } from "react-router-dom";
/**Vado a riutilizzare lo stile del componente info data source */
import useStyles from "../../dataSourcesDetails/InfoDataSource/index";

import { CURRENT_ENTITY } from '../../../states/actions'


import { useContext } from "react";
import { AuthContext } from "../../../states/context.js";
import { dataSourceType } from '../../../config/constants' 
import { getEntityDetails } from '../../../services/backend/dataSourcesBackend'

const InfoEntity = memo(({ entityData }) => {

  const ctx = useContext(AuthContext)
  const { dataSourceDispatch, dataSourceContext, entityDispatch, entityContext } = ctx
  
  const classes = useStyles();
  const navigate = useNavigate();

  const updateEntityHandler = async (id) => {
    const data = {};
    const response = await updateEntity(id, data);
    console.log(response);
  };

  const deleteEntityHandler = async (id) => {
    const response = await deleteEntity(id);
    console.log(response);
  };

  useEffect(() => {
    /**Get Entity Details */
    const myEntity = getEntityDetails(7)
    entityDispatch({type: CURRENT_ENTITY, payload: myEntity.data})
  }, []);

  const handleEdit = () => {
    console.log(entityContext)    
    if (entityContext.currentEntity.dataSourceType == dataSourceType.DATABASE)
      navigate(`/entities/editDatabase/`);
    else 
      navigate(`/entities/editPlmCad/`);

  }

  const goToSpecificDataSource = useCallback(() => {
    navigate(`/dataSources/${String(entityContext.currentEntity.dataSourceID)}`);
  }, [navigate]);

  return (
    <React.Fragment>
      <Box display={"flex"} alignItems="center">
        <Button
          className={classes.backButton}
          size="small"
          variant="contained"
          onClick={goToSpecificDataSource}
        >
          <ArrowBackIcon size="large" color="white" />
        </Button>
        <Typography variant="h5" fontWeight={500} color="primary">
          Entity Details
        </Typography>
      </Box>
      <Accordion className={classes.collapseStyles}>
        <AccordionSummary
          sx={{ border: "hidden !important" }}
          expandIcon={<ExpandMoreIcon />}
        >
          <Typography variant="h6">...</Typography>
        </AccordionSummary>
        
        
        <AccordionDetails>
          <form>
            
            <Grid
              container
              display="flex"
              spacing={2}
              padding={1}
              marginTop="0.5rem"
            >
              <Grid item xs={6} md={3}>
                <FormGroup>
                  <TextField
                    label="Description"
                    defaultValue={entityData.description}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </FormGroup>
              </Grid>
              
              <Grid item xs={6} md={3}>
                <FormGroup>
                  <TextField
                    label="Entity Type"
                    defaultValue={entityData.entityType}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </FormGroup>
              </Grid>
              
              <Grid item xs={6} md={3}>
                <FormGroup>
                  <TextField
                    label="Label"
                    defaultValue={entityData.label}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </FormGroup>
              </Grid>
              
              <Grid item xs={6} md={3}>
                <FormGroup>
                  <TextField
                    label="Origin Name"
                    defaultValue={entityData.originName}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </FormGroup>
              </Grid>
              <Grid item xs={6} md={3}>
                <FormGroup>
                  <TextField
                    label="Origin Type"
                    defaultValue={entityData.originType}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={3}>
                <FormGroup>
                  <TextField
                    label="Domain"
                    defaultValue={entityData.domain}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </FormGroup>
              </Grid>


              <Grid item xs={6} md={6}>
                <FormGroup>
                  <TextField
                    label="Business Object"
                    defaultValue={entityData.businessObject}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </FormGroup>
              </Grid>
              {/*               
              <Grid item xs={6} md={6}>
                <FormGroup>
                  <TextField
                    label="OriginNavigationUrl"
                    defaultValue={entityData.OriginNavigationUrl}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </FormGroup>
              </Grid> 
              */}
              <Grid item xs={12} md={12}>
                <FormGroup>
                  <TextField
                    label="Origin Direct Query"
                    defaultValue={entityData.OriginDirectQuery}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </FormGroup>
              </Grid>
            </Grid>
            <div className={classes.containerButton}>
              <Button
                size="large"
                className={classes.buttonTest}
                variant="contained"
              >
                Test
              </Button>
              <div className={classes.containerButtonRight}>
                <Button
                  size="large"
                  startIcon={<EditIcon />}
                  className={classes.editButton}
                  onClick={handleEdit}
                  variant="contained"
                >
                  Edit
                </Button>
                <Button
                  size="large"
                  startIcon={<DeleteOutlineIcon />}
                  color="error"
                  variant="contained"
                  onClick={deleteEntityHandler}
                >
                  Trash
                </Button>
              </div>
            </div>
          </form>
        </AccordionDetails>
      </Accordion>
    </React.Fragment>
  );
});

export default InfoEntity;
