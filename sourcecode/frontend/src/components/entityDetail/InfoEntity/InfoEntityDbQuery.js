import React, { memo, useEffect, useCallback, useState } from "react";
import {
  Grid,
  Typography,
  TextField,
  FormGroup,
  Button,
  Box,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  TextareaAutosize
} from "@mui/material";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import EditIcon from "@mui/icons-material/Edit";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {updateEntity,deleteEntity,} from "../../../services/backend/dataSourcesBackend";
import { useNavigate } from "react-router-dom";
/**Vado a riutilizzare lo stile del componente info data source */
import useStyles from "../../dataSourcesDetails/InfoDataSource/index";
import * as Yup from "yup";
import { useFormik } from "formik";

import { CURRENT_ENTITY } from '../../../states/actions'


import { useContext } from "react";
import { AuthContext } from "../../../states/context.js";
import { dataSourceType } from '../../../config/constants' 
import { getEntityDetails } from '../../../services/backend/dataSourcesBackend'
import CheckIcon from "@mui/icons-material/Check";
import { format } from 'sql-formatter';


const InfoEntity = memo(({ generalInfo }) => {


  const ctx = useContext(AuthContext)
  const { dataSourceDispatch, dataSourceContext, entityDispatch, entityContext } = ctx

  const classes = useStyles();
  const navigate = useNavigate();

  const [editMode, setEditMode] = useState(true);

  const initialValues = {
    entityType: generalInfo.entityType,
    originType: generalInfo.originType,
    label: generalInfo.label, // per dataSource type DB
    description: generalInfo.description, // per dataSource type DB
    // originName: generalInfo.originName, // per dataSource type DB - solo entity table
    originDirectQuery: generalInfo.originDirectQuery, // per dataSource type DB - entity Query
    // domain: "", per datasource type CAD o PLM
    // businessObject: "", per datasource type CAD o PLM
  }

  const validationSchema = Yup.object({
    entityType: Yup.string().required(),
    originType: Yup.string().required(),
    label: Yup.string().required(),
    description: Yup.string().required(),
    originDirectQuery: Yup.string().required(),
  })

  const onSubmitHandler = async () => {

    setEditMode(!editMode); 

    if (editMode) {
      console.log("onSubmitHandler called EDIT MODE")
    }
    else {
      console.log("onSubmitHandler called NOT EDIT MODE")
    }


  };

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: validationSchema,
    onSubmit: onSubmitHandler,
  });





  const deleteEntityHandler = async (id) => {
    console.log("deleteEntityHandler called");
    const response = await deleteEntity(id);
  };




  // useEffect(() => {
  //   /**Get Entity Details */
  //   const myEntity = getEntityDetails(7)
  //   entityDispatch({type: CURRENT_ENTITY, payload: myEntity.data})
  // }, []);



  
const renderSaveEditButton = () => {
  if (editMode) return (
    <Button
      size="large"
      startIcon={<EditIcon />}
      className={classes.editButton}
      variant="contained"
      onClick={formik.onSubmit}
      type="submit"

      >
      Edit
    </Button>
  ) 
  else return (
    <Button
      size="large"
      color="success"
      startIcon={<CheckIcon />}
      className={classes.saveButton}
      onClick={formik.onSubmit}
      variant="contained"
      type="submit"
      >
      Save
    </Button>
  )}


  const goToSpecificDataSource = useCallback(() => {
    navigate(`/dataSources/${String(entityContext.currentEntity.dataSourceID)}`);
  }, [navigate]);

  return (
    <React.Fragment>
      <Box display={"flex"} alignItems="center">
        <Button
          className={classes.backButton}
          size="small"
          variant="contained"
          onClick={goToSpecificDataSource}
        >
          <ArrowBackIcon size="large" color="white" />
        </Button>
        <Typography variant="h5" fontWeight={500} color="primary">
          Entity Details
        </Typography>
      </Box>
      <Accordion className={classes.collapseStyles}>
        <AccordionSummary
          sx={{ border: "hidden !important" }}
          expandIcon={<ExpandMoreIcon />}
        >
          <Typography variant="h6">...</Typography>
        </AccordionSummary>
        
        
        <AccordionDetails>
          <form onSubmit={formik.handleSubmit}>
            
            <Grid
              container
              display="flex"
              spacing={2}
              padding={1}
              marginTop="0.5rem"
            >
              <Grid item xs={6} md={4}>
                <FormGroup>
                  <TextField
                    label="Entity Type"
                    value={formik.values.entityType}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={4}>
                <FormGroup>
                  <TextField
                    label="Origin Type"
                    value={formik.values.originType}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={4}>
                <FormGroup>
                  <TextField
                    label="Label"
                    id="label"
                    onChange={formik.handleChange}
                    value={formik.values.label}
                    InputProps={{
                      readOnly: editMode,
                    }}
                    error={
                      formik.touched.label && Boolean(formik.errors.label)
                    }
                  />
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={12}>
                <FormGroup>
                  <TextareaAutosize
                    id="originDirectQuery"

                    maxRows={5}
                    aria-label="maximum height"
                    // placeholder="Copy and paste query"
                    // defaultValue="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                    //     ut labore et dolore magna aliqua."
                    // onChange = { e => handleFieldsUpdate ("originDirectQuery", e)}
                    // error={
                    //   formik.touched.originDirectQuery && Boolean(formik.errors.originDirectQuery)
                    // }

                    value={format(formik.values.originDirectQuery)}
                    style={{ height: 150, fullWidth: '100%' }}
                  />
                </FormGroup>
              </Grid>
              
            {/*   <Grid item xs={6} md={3}>
                <FormGroup>
                  <TextField
                    label="Origin Direct Query"
                    value={formik.values.originDirectQuery}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </FormGroup>
              </Grid> */}

              <Grid item xs={12} md={12}>
                <FormGroup>
                  <TextField
                    label="Description"
                    id="description"
                    onChange={formik.handleChange}
                    value={formik.values.description}
                    InputProps={{
                      readOnly: editMode,
                    }}
                    error={
                      formik.touched.description && Boolean(formik.errors.description)
                    }
                  />
                </FormGroup>
              </Grid>


            {/*

              <Grid item xs={6} md={3}>
                <FormGroup>
                  <TextField
                    label="Domain"
                    defaultValue={entityData.domain}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </FormGroup>
              </Grid>


              <Grid item xs={6} md={6}>
                <FormGroup>
                  <TextField
                    label="Business Object"
                    defaultValue={entityData.businessObject}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </FormGroup>
              </Grid>

              <Grid item xs={12} md={12}>
                <FormGroup>
                  <TextField
                    label="Origin Direct Query"
                    defaultValue={entityData.originDirectQuery}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </FormGroup>
              </Grid>

            */}
            
            </Grid>

            <div className={classes.containerButton}>
              <Button
                size="large"
                className={classes.buttonTest}
                variant="contained"
              >
                Test
              </Button>
              <div className={classes.containerButtonRight}>
                {renderSaveEditButton()}
                <Button
                  size="large"
                  startIcon={<DeleteOutlineIcon />}
                  color="error"
                  variant="contained"
                  onClick={deleteEntityHandler}
                >
                  Trash
                </Button>
              </div>
            </div>
          </form>
        </AccordionDetails>
      </Accordion>
    </React.Fragment>
  );
});

export default InfoEntity;
