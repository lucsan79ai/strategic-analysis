import React, { useState, useCallback, memo, useEffect } from "react";
import { Button, Chip, Grid, TextField, Typography } from "@mui/material";
import {
  getTopics,
  insertTopic,
  removeTopic,
} from "../../../services/backend/dataSourcesBackend";
import DeleteOutline from "@mui/icons-material/DeleteOutline";
import useStyles from "./index";

const TagComponent = memo(({ isNew, entityData }) => {
  const [tagsCard, setTag] = useState([]);
  const [tagLabel, setTagLabel] = useState("");
  const classes = useStyles();

  useEffect(() => {
    
    (async () => {
      if (!isNew) {
        const response = await getTopics(entityData.id);
        setTag(response.data);
        console.log(response);
      }
      else console.log('nuova entity')
    })();
  }, [entityData.id]);

  const handleDelete = useCallback(
    async (id) => {
      const newArray = tagsCard.filter((item) => item.id !== id);
      setTag(newArray);
      const response = await removeTopic(entityData.id);
      console.log(response);
    },
    [tagsCard, entityData.id]
  );

  const onSubmit = async (e) => {
    e.preventDefault();
    if(tagLabel.trim()==="") return
    const id = Math.random();
    const response = await insertTopic(entityData.id, tagLabel);
    console.log(response);
    setTag((preValue) => [...preValue, { id: id, label: tagLabel }]);
    setTagLabel("");
  };

  return (
    <React.Fragment>
      <Typography variant="h6" color="primary" mt={2}>
        Entity Topics
      </Typography>
      <Grid container display="flex" spacing={2} padding={1}>
        {tagsCard.map((tag) => (
          <Grid item key={tag.id}>
            <Chip
              label={`${tag.label}`}
              onDelete={() => {
                handleDelete(tag.id);
              }}
              deleteIcon={<DeleteOutline />}
            />
          </Grid>
        ))}
      </Grid>
      <form className={classes.formContainer} onSubmit={onSubmit}>
        <TextField
          type="text"
          value={tagLabel}
          onChange={(e) => setTagLabel(e.target.value)}
          className={classes.textField}
          label="New Topic"
        ></TextField>
        <Button
          variant="contained"
          color="success"
          className={classes.buttonSubmit}
          type="submit"
        >
          Add Topic
        </Button>
      </form>
    </React.Fragment>
  );
});

export default TagComponent;
