import { makeStyles } from "@mui/styles";

export default makeStyles((theme) => ({
  formContainer: {
    display: "flex",
    alignItems: "center",
    margin:"1rem 0",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
    },
  },

  buttonSubmit: {
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    },
  },

  textField: {
    marginRight: "1rem !important",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      marginRight: "0 !important",
      marginBottom: "1rem !important",
    },
  },
}));
