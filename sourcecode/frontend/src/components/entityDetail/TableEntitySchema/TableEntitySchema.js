import React, { memo, useState, useCallback } from "react";
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TablePagination,
    Typography,
    Paper,
    Switch,
    TextField,
    Button
} from "@mui/material";
import { CURRENT_ENTITY_SCHEMA_ARRAY } from "../../../states/actions";
import { dataHead } from "./dataTable";
import useStyles from "./index";
import DropDownMenu from "../DropDownMenu/DropDownMenu";

import { useContext } from "react";
import { AuthContext } from "../../../states/context";



const TableEntitySchema = memo(({ entityData, colorHeader, handleDropdownChange }) => {

  const ctx = useContext(AuthContext)
  const { dataSourceDispatch, dataSourceContext, entityDispatch, entityContext } = ctx

 
  const pages = [5, 10, 25];
  const [page, setPage] = useState(0);
  const [rowsForPage, setRowsForPage] = useState(pages[0]);
  const classes = useStyles();

  const handleChangePage = useCallback((event, newPage) => {
    setPage(newPage);
  }, []);


  const handleLabelChange = (e, item) => {
    console.log('item.id == ' + item.id)
    console.log('la nuova label è cambiata in: ' + e.target.value)

    const foundIndex = entityData.findIndex(element => element.id == item.id);
    const newItem =  entityData[foundIndex];
    newItem.label = e.target.value
    entityData[foundIndex] = newItem

    entityDispatch({type: CURRENT_ENTITY_SCHEMA_ARRAY, payload: entityData});

  }
  
  const handleRowsPerPage = useCallback((event) => {
    setRowsForPage(+event.target.value);
    setPage(0);
  }, []);

  return (
    <React.Fragment>
      <TableContainer component={Paper} style={{ marginBottom: "5rem" }}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead sx={{ backgroundColor: colorHeader }}>
            <TableRow>
              {dataHead.map((cell) => {
                return (
                  <TableCell className={classes.tableHeader} key={cell.field}>
                    <Typography
                      variant="h6"
                      color="white"
                      fontWeight="500"
                      fontSize="1.2rem"
                    >
                      {cell.title}
                    </Typography>
                  </TableCell>
                ); 
              })}
            </TableRow>
          </TableHead>
          <TableBody>
            {entityContext.currentEntity.currentSchemaArray?.slice(
              page * rowsForPage,
              page * rowsForPage + rowsForPage
            ).map((item) => (
              <TableRow key={item.id} hover>
                <TableCell>{item.fieldName}</TableCell>
                <TableCell>{item.fieldType}</TableCell>
                <TableCell>
                <TextField
                    id="item.label"
                    onChange={e => handleLabelChange(e, item)}
                    variant="standard"
                    value={item.label}
                    // InputProps={{
                    //   // readOnly: editMode,
                    //   readOnly: true,
                    // }}
                  />
                </TableCell>
                <TableCell><Switch defaultChecked /></TableCell>
                <TableCell>
                  {" "}
                   <DropDownMenu handleDropdownChange={handleDropdownChange} item={item} />
                 </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <TablePagination
          component="div"
          count={entityData?.length - 1}
          rowsPerPageOptions={pages}
          page={page}
          rowsPerPage={rowsForPage}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleRowsPerPage}
        />
                         
        <div>
          <Button 
              fullWidth 
              variant="contained">
                Save</Button>
        </div>

      </TableContainer>

    </React.Fragment>
  );
});

export default TableEntitySchema;
