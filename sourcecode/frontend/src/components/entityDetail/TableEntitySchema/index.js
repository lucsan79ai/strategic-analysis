import { makeStyles } from "@mui/styles";

export default makeStyles((theme) => ({
  tableHeader: {
    color:theme.palette.primary.main,
  },
  cellSucces:{
    color:theme.palette.success.light,
  },
  cellWarning:{
    color:theme.palette.warning.main,
  },

  buttonColor:{
    backgroundColor:"#E0E0E0 !important",
    color:"gray !important",
    minWidth:"100%"
  }
  

}));
