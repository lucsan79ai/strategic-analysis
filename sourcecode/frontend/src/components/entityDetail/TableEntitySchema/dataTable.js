export const dataHead = [
    {
      title: "Field Name",
      field: "fieldName",
    },
    {
      title: "Field Type",
      field: "fieldType",
    },
    {
      title:"Label",
      field:"label"
    },
    {
      title: "Visible",
      field: "Visible",
    },

    {
      title: "Key",
      field: "key",
    },
  ];
  