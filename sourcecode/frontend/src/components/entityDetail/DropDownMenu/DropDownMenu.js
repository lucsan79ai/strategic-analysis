import * as React from "react";
import { memo,useState,useRef } from "react";
import Button from "@mui/material/Button";
import ButtonGroup from "@mui/material/ButtonGroup";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import ClickAwayListener from "@mui/material/ClickAwayListener";
import Grow from "@mui/material/Grow";
import Paper from "@mui/material/Paper";
import Popper from "@mui/material/Popper";
import MenuItem from "@mui/material/MenuItem";
import MenuList from "@mui/material/MenuList";

const options = ["_", "ID", "Code",  "Description", "Project" ];

const DropDownMenu=memo(( {/* handleDropdownChange, */ item} )=> {

  //Open and Close the DropDownMenu
  const [open, setOpen] = useState(false);
  const anchorRef =useRef(null);
  //Item selected
  const [selectedIndex, setSelectedIndex] = useState("");



  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };


 const handleMenuItemClick = (event, index) => {
  setSelectedIndex(index);
  setOpen(false);
};


  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  };



  //********************************************** TO DO ----> Qua bisogna eseguire la funzione ! *******************************************

  React.useEffect(()=>{
    // console.log("Hai selezionata --> " + options[selectedIndex])
    // handleDropdownChange(options[selectedIndex],item)


  },[ selectedIndex])



  return (
    <React.Fragment>
      <ButtonGroup /* variant="contained" */ ref={anchorRef} >
        <Button >{options[selectedIndex]}</Button>
        <Button size="small" onClick={handleToggle}>
          <ArrowDropDownIcon />
        </Button>
      </ButtonGroup>
      <Popper
        open={open}
        anchorEl={anchorRef.current}
        role={undefined}
        transition
        disablePortal
        style={{ zIndex: 15 }}
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin:
                placement === "bottom" ? "center top" : "center bottom",
            }}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList >
                  {options.map((option, index) => (
                    <MenuItem
                      key={option}
                      selected={index === selectedIndex}
                      onClick={(event) => handleMenuItemClick(event, index)}
                    >
                      {option}
                    </MenuItem>
                  ))}
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </React.Fragment>
  );
})


export default DropDownMenu




