import { makeStyles } from "@mui/styles";

export default makeStyles((theme) => ({
  title: {
    display: "flex",
    alignItems: "start",
    justifyContent: "start",
    fontWeight: 600,
    color: "white",
    fontSize: "1.5rem",
  },
}));
