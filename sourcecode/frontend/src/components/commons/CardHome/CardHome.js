import React from "react";
import { Card, CardContent, Typography } from "@mui/material";
import useStyles from "./index";

const CardHome = ({ title, secondTitle, content, colorCard }) => {

  const classes = useStyles();

  return (
    <Card
      sx={{
        cursor:"pointer",
        padding: "1rem",
        minHeight: "300px",
        backgroundColor: `${colorCard}`,
      }}
    >
      <div className={classes.title}>{title}</div>
      <hr />
      <CardContent>
        <div className={classes.title}>{secondTitle}</div>
        <Typography variant="subtitle" color="white" fontWeight={500}>
          {content}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default CardHome;
