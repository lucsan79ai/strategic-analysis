import { makeStyles } from "@mui/styles";

export default makeStyles((theme)=>({

    title: {
        padding: '20px',
    },
    subtitle: {
        marginLeft: '20px',
        marginTop: '5px',
        paddingBottom: '20px'
    }

/*     paper:{
        marginBottom:'10px',
        padding:'18px',
        display:'flex',
        justifyContent:'space-between',
        alignItemns:'center',
        [theme.breakpoints.down("md")]:{
            '& .MuiButton-text':{
                fontSize:'0.75rem',
            }
          
        }
    },

    buttonClass:{
        [theme.breakpoints.down("md")]:{
                fontSize:'0.75rem',
        
        }
    }
 */}))