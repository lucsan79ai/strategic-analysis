import React from "react";
import { Paper, Typography, Button, Grid  } from "@mui/material";
import useStyles from "./index.js";

const TitleSubtitle = ({ title, subtitle }) => {
  const classes = useStyles();


  return (

          <Paper >
            <Typography className={classes.title} variant="h4" color="primary">
            <div>{title}</div> 
            </Typography>
            <Typography className={classes.subtitle} variant="h6" color="primary">
              {subtitle}
            </Typography>
          </Paper>

  );
};

export default TitleSubtitle;
