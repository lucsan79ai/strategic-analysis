import { makeStyles } from "@mui/styles";

export default makeStyles((theme)=>({

    containerHeader:{
        margin:"2rem 0",
        display:'flex',
        flexDirection:"column",
        justifyContent:"start",
        alignItemns:'start',
        [theme.breakpoints.down("md")]:{
            '& .MuiButton-text':{
                fontSize:'0.75rem',
            }
        }
    },

    buttonClass:{
        [theme.breakpoints.down("md")]:{
                fontSize:'0.75rem',
        
        }
    },

    
}))