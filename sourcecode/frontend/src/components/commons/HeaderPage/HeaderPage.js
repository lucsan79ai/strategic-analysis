import React from "react";
import { Box, Typography } from "@mui/material";
import useStyles from "./index.js";

const HeaderPage = ({ title }) => {
  const classes = useStyles();

  return (
    <div className={classes.containerHeader}>
      <Box
        display={"flex"}
        alignItems="center"
        marginBottom={"1rem"}

>
        <Typography
          variant="h5"
          color="primary"
          fontWeight={500}
        >
          {title}
        </Typography>
      </Box>

      <Typography>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus,
        nulla ut commodo sagittis.
      </Typography>
    </div>
  );
};

export default HeaderPage;
