import React from "react";
import {
  Box,
  TableRow,
  TableCell,
  TableHead,
  TableSortLabel,
  Typography,
} from "@mui/material";
import { useTheme } from "@emotion/react";


const visuallyHidden = {
  border: 0,
  margin: -1,
  padding: 0,
  width: "1px",
  height: "1px",
  overflow: "hidden",
  position: "absolute",
  whiteSpace: "nowrap",
  clip: "rect(0 0 0 0)",
};

const TableHeadCostum = ({
  order,
  orderBy,
  colorHeader,
  headLabel,
  onSort,
}) => {
  const theme = useTheme();



  return (
    <TableHead sx={{ backgroundColor: colorHeader }}>
      <TableRow>
        {headLabel.map((headCell) => (
          <TableCell
            color={theme.palette.primary.main}
            key={headCell.id}
            align={headCell.align || "left"}
            sortDirection={orderBy === headCell.id ? order : false}
            sx={{ width: headCell.width, minWidth: headCell.minWidth }}
          >
            {onSort ? (
              <TableSortLabel
                hideSortIcon
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : "asc"}
                onClick={() => onSort(headCell.id)}
                sx={{ textTransform: "capitalize", }}
              >
                <Typography
                  variant="h6"
                  color="white"
                  fontWeight="500"
                  fontSize="1.2rem"
                >
                  {headCell.label}
                </Typography>

                {orderBy === headCell.id ? (
                  <Box sx={{ ...visuallyHidden }}>
                    {order === "desc"
                      ? "sorted descending"
                      : "sorted ascending"}
                  </Box>
                ) : null}
              </TableSortLabel>
            ) : (
              headCell.label
            )}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

export default TableHeadCostum;
