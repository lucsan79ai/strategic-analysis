import { makeStyles } from "@mui/styles";

export default makeStyles((theme) => ({
  containerSearchAdd: {
    display: "flex",
    alignItemns: "center",
    justifyContent: "space-between",
    margin: "2rem 0",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
    },
  },

  formContainer: {
    display: "flex",
    alignItems: "center",
    height: "2.5rem",
    [theme.breakpoints.down("sm")]: {
      marginBottom: "2rem",
    },
  },

  textField: {
    height: "100%",
  },

  input: {
    maxHeight: "100%",
  },

  searchButton: {
    pointerEvents:"none",
    marginLeft: "1rem !important",
  },

 
}));
