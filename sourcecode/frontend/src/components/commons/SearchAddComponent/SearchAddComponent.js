import React, { memo } from "react";

//@mui
import { Button, TextField } from "@mui/material";

//hook
import { useTheme } from "@emotion/react";
import useStyles from "./index";

//components
import AddBoxIcon from "@mui/icons-material/AddBox";
import SearchIcon from "@mui/icons-material/Search";

const SearchAddComponent = memo(({ filterName,onFilterName,placeHolder, addButton,deleteButton=false }) => {
  const classes = useStyles();
  const theme = useTheme()

  return (
    <div className={classes.containerSearchAdd}>
      <form className={classes.formContainer}>
        <TextField
          className={classes.textField}
          value={filterName}
          onChange={(event) => onFilterName(event.target.value)}
          placeholder={placeHolder}

          InputProps={{
            className: classes.input,
          }}
        ></TextField>
        <Button style={{backgroundColor:theme.palette.secondary.main}} className={classes.searchButton}  variant="contained">
          <SearchIcon size="large" color="white" />
        </Button>
      </form>

      {!deleteButton && <Button
        variant="contained"
        color="primary"
        onClick={addButton}
        className={classes.addButton}
        startIcon={<AddBoxIcon />}
      >
        Add
      </Button>}
    </div>
  );
});

export default SearchAddComponent;
