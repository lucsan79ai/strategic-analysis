
import React, { memo, useState, useCallback } from "react";

// @mui
import { Table,Paper, TableBody,TableCell,TableContainer,TableRow,TablePagination,Button,} from "@mui/material";
import PreviewIcon from "@mui/icons-material/Preview";

//hook
import { dataHead } from "./dataTable";
import useTable,{getComparator} from "../../../customHook/useTable";
import { useNavigate } from "react-router-dom";
import { useTheme } from "@emotion/react";
import useStyles from "./index.js";

//components
import SearchAddComponent from "./../../commons/SearchAddComponent/SearchAddComponent"
import TableHeadCustom from "../../commons/table/TableHeadCostum/TableHeadCostum"

import { 
  CURRENT_ENTITY_INIT, 
  CURRENT_ENTITY_DATA_SOURCE_ID,
  CURRENT_ENTITY_DATA_SOURCE_TYPE
} from "../../../states/actions"

import { useContext } from "react";
import { AuthContext } from "../../../states/context.js";

import { dataSourceType } from '../../../config/constants'


const TableDataSourceDetails = memo(({  dataSource, dataSourceId }) => {

  const {  
    page,
    pages,
    setPage,
    order,
    orderBy,
    rowsForPage,
    //
    onSort,
    onChangePage,
    onChangeRowsPerPage} = useTable({defaultOrderBy:'label'})

    //hook
  const theme=useTheme()
  const classes = useStyles();
  const navigate = useNavigate();
  const [filterName, setFilterName] = useState('');

  const ctx = useContext(AuthContext)
  const { dataSourceDispatch, dataSourceContext, entityDispatch, entityContext } = ctx

  const goToEntities = useCallback((id)=>{
    navigate(`/entity/${id}`)
  },[navigate])



  const handleAddEntity = () => {
    console.log('handleAddEntity -- Current Entity: ', entityContext)
    // console.log('Current Datasource: ', dataSourceContext)
    
    if (entityContext.currentEntity.dataSourceType == dataSourceType.DATABASE) 
      navigate(`/entities/newDatabase/${dataSourceId}`)
    else {
      navigate(`/entities/newPlmCad/${dataSourceId}`)
    }
  }
  
  const handleFilterName = (filterName) => {
    setFilterName(filterName);
    setPage(0);
  };

  const dataFiltered = applySortFilter({
    tableData:dataSource.dataSourceEntity,
    comparator: getComparator(order, orderBy),
    filterName,
  }); 
  

  console.log(dataFiltered)
  

   const isNotFound = ( !dataFiltered.length && !! filterName ) || ( !dataFiltered.length);

  

  return (
    <React.Fragment>
    <SearchAddComponent placeHolder={"Search Entities.."} filterName={filterName} onFilterName={handleFilterName} addButton={handleAddEntity}/>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHeadCustom
                  order={order}
                  orderBy={orderBy}
                  colorHeader={theme.palette.secondary.light}
                  headLabel={dataHead}
                  onSort={onSort}
                />


          <TableBody>
            {dataFiltered.slice(
              page * rowsForPage,
              page * rowsForPage + rowsForPage
            ).map((item) => (
              <TableRow key={item.id} hover>
                <TableCell>{item.label}</TableCell>
                <TableCell>{item.description}</TableCell>
                <TableCell>{item.entityType}</TableCell>
                <TableCell>{item.originName}</TableCell>
                <TableCell>{item.originType} </TableCell>
                <TableCell>
                  <Button
                    startIcon={<PreviewIcon />}
                    onClick={() => goToEntities(item.id)}
                    className={classes.buttonColor}
                    variant="contained"
                  >
                    Views Details
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <TablePagination
          component="div"
          count={dataSource?.dataSourceEntity.length - 1}
          rowsPerPageOptions={pages}
          page={page}
          rowsPerPage={rowsForPage}
          onPageChange={onChangePage}
          onRowsPerPageChange={onChangeRowsPerPage}
        />
      </TableContainer>
    </React.Fragment>
  );
});

export default TableDataSourceDetails;

// ----------------------------------------------------------------------

function applySortFilter({ tableData, comparator, filterName }) {
  const stabilizedThis = tableData?.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });

  tableData = stabilizedThis.map((el) => el[0]);
 
  
  if (filterName) {
    tableData = tableData.filter((item) => item.description!==null && item.description.toLowerCase().indexOf(filterName.toLowerCase()) !== -1);
  } 

  return tableData;
}