import { makeStyles } from "@mui/styles";

export default makeStyles((theme)=>({

    containerButton:{
        display:"flex",
        alignItems:"center",
        margin:"1rem 0",
        justifyContent:"space-between",
        [theme.breakpoints.down("sm")]: {
            flexDirection: "column",
          },
    },
    buttonTest:{
        backgroundColor:"#616161 !important",
        color:"white",
        fontWeight:"600",
        [theme.breakpoints.down("sm")]: {
            width:"100%"
        },
    },
    containerButtonRight:{
        display:"flex",
        [theme.breakpoints.down("sm")]: {
            flexDirection: "column",
            width:"100%"
        },
    },
    editButton:{
        backgroundColor:"#F4D636 !important",
        marginRight:"2rem !important",
        [theme.breakpoints.down("sm")]: {
            margin:"1rem 0 !important",
        },
    },
    saveButton:{
        marginRight:"2rem !important",
        [theme.breakpoints.down("sm")]: {
            margin:"1rem 0 !important",
        },
    },
    backButton: {
        backgroundColor: "#616161 !important",
        marginRight: "1rem !important",
        borderRadius:"3rem !important",
        minWidth:"0 !important"
      },
    
      collapseStyles:{
          marginTop:"1rem",
          width:"100%",
          borderRadius:"2rem !important",
          '&::before': {
            height: "0px !important",
          }
      }
    

}))