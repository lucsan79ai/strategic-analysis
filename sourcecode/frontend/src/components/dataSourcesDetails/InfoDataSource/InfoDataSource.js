import React, { useState, memo, useCallback } from "react";
import {
  Grid, 
  Typography,
  TextField,
  FormGroup,
  Button,
  Box,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  FormControl,
  InputLabel,
  Select,
  MenuItem


} from "@mui/material";
import * as Yup from "yup";
import { useFormik } from "formik";
import {
  deleteDataSource,
  testConnection,
  updateDataSource,
} from "../../../services/backend/dataSourcesBackend";
import { useNavigate } from "react-router-dom";
import useStyles from "./index";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import CheckIcon from "@mui/icons-material/Check";
import EditIcon from "@mui/icons-material/Edit";

const InfoDataSource = memo(({ dataSource }) => {
  const classes = useStyles();
  const navigate = useNavigate();
  
  //@deprecated
  const [editMode, setEditMode] = useState(true);

  const initialValues = {
    dataSourceType: dataSource.dataSourceType,
    description: dataSource.description,
    connectionType: dataSource.connectionType,
    serverName: dataSource.serverName,
    sourceName: dataSource.sourceName,
    username: dataSource.username,
    password: dataSource.password,
    status: dataSource.status,
    port: dataSource.port
  };

  const validationSchema = Yup.object({
    dataSourceType: Yup.string().required(),
    description: Yup.string(),
    connectionType: Yup.string().required(),
    serverName: Yup.string().required(),
    sourceName: Yup.string().required(),
    username: Yup.string().required(),
    password: Yup.string().required(),
    status: Yup.string().required(),
    port: Yup.string().required(),
    // dataSourceType: Yup.string(),
    // description: Yup.string(),
    // connectionType: Yup.string(),
    // serverName: Yup.string(),
    // sourceName: Yup.string(),
    // username: Yup.string(),
    // password: Yup.string(),
    // status: Yup.string(),
    // port: Yup.string()
  });

  const deleteDataSourceHandler = async (id) => {
    const response = await deleteDataSource(id);
    console.log(response);
  };

  const testDataSourceHandler = async (id) => {
    const response = await testConnection(id);
    console.log(response);
  };

  
  const goToDataSources = useCallback(() => {
    navigate("/dataSources");
  }, [navigate]);

  const onSubmitHandler = async () => {
    
    if (editMode) {
      console.log("onSubmitHandler called", formik.values)
      const data = {};
      const response = await updateDataSource(dataSource.id, data);
      console.log(response);
    }
    setEditMode(!editMode); 
  };

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: validationSchema,
    onSubmit: onSubmitHandler,
  });

  // const handleEdit = (event) => {
  //   console.log('handleEdit called')
  //   // navigate("/dataSources/edit")
  //   setEditMode(!editMode); 

  // }

  
  const handleStatusUpdate = (e) => {
    formik.setFieldValue("status", e.target.value )
} 

const renderSaveEditButton = () => {
  if (editMode) return (
    <Button
      size="large"
      startIcon={<EditIcon />}
      className={classes.editButton}
      variant="contained"
      onClick={formik.onSubmit}
      type="submit"
      >
      Edit
    </Button>
  ) 
  else return (
    <Button
      size="large"
      color="success"
      startIcon={<CheckIcon />}
      className={classes.saveButton}
      variant="contained"
      type="submit"
      >
      Save
    </Button>
  )}


  return (
    <React.Fragment>
      <Box display={"flex"} alignItems="center" marginTop={3}>
        <Button
          className={classes.backButton}
          size="small"
          variant="contained"
          onClick={goToDataSources}
        >
          <ArrowBackIcon size="large" color="white" />
        </Button>
        <Typography variant="h5" color="primary" fontWeight={500}>
          Data Source Details
        </Typography>
      </Box>
      <Accordion className={classes.collapseStyles}>
        <AccordionSummary
          sx={{ border: "hidden !important" }}
          expandIcon={<ExpandMoreIcon />}
        >
          <Typography variant="h6">...</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <form onSubmit={formik.handleSubmit}>
            <Grid
              container
              display="flex"
              spacing={2}
              padding={1}
              marginTop="0.5rem"
            >
              
              <Grid item xs={6} md={3}>
                <FormGroup>
                  <TextField
                    id="dataSourceType"
                    onChange={formik.handleChange}
                    value={formik.values.dataSourceType}
                    label="dataSourceType"
                    InputProps={{
                      // readOnly: editMode,
                      readOnly: true,
                    }}

                  />
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={3}>
                <FormGroup>
                  <TextField
                    label="connectionType"
                    id="connectionType"
                    onChange={formik.handleChange}
                    value={formik.values.connectionType}
                    InputProps={{
                      // readOnly: editMode,
                      readOnly: true,
                    }}
                  />
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={3}>
                <FormGroup>
                  <TextField
                    id="sourceName"
                    label="sourceName"
                    onChange={formik.handleChange}
                    value={formik.values.sourceName}
                    InputProps={{
                      readOnly: editMode,
                    }}
                    error={
                      formik.touched.sourceName && Boolean(formik.errors.sourceName)
                    }
                  />
                </FormGroup>
              </Grid>

{/*               <Grid item xs={6} md={3}>
                <FormGroup>
                  <TextField
                    label="status"
                    id="status"
                    onChange={formik.handleChange}
                    value={formik.values.status}
                    InputProps={{
                      readOnly: editMode,
                    }}
                  />
                </FormGroup>
              </Grid> */}

              
              <Grid item xs={6} md={3}>
                <FormControl fullWidth>
                <InputLabel>status</InputLabel>
                  <Select
                    labelId="status"
                    id="status"
                    type="text"
                    value={formik.values.status}
                    label="Database type"
                    onChange = { e => handleStatusUpdate (e)}
                    disabled = {editMode}
                    error={
                      formik.touched.status &&
                      Boolean(formik.errors.status)
                    } 
                  >
                    <MenuItem value={"PUBLIC"}>PUBLIC</MenuItem>
                    <MenuItem value={"PRIVATE"}>PRIVATE</MenuItem>
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={12}>
                <FormGroup>
                  <TextField
                    id="description"
                    onChange={formik.handleChange}
                    value={formik.values.description}
                    label="description"
                    InputProps={{
                      readOnly: editMode,
                    }}
                    error={
                      formik.touched.description && Boolean(formik.errors.description)
                    }
                  />
                </FormGroup>
              </Grid>


              <Grid item xs={6} md={3}>
                <FormGroup>
                  <TextField
                    id="serverName"
                    label="serverName"
                    onChange={formik.handleChange}
                    value={formik.values.serverName}
                    InputProps={{
                      readOnly: editMode,
                    }}
                    error={
                      formik.touched.serverName && Boolean(formik.errors.serverName)
                    }
                  />
                </FormGroup>
              </Grid>
              


              <Grid item xs={6} md={3}>
                <FormGroup>
                  <TextField
                    id="username"
                    label="username"
                    onChange={formik.handleChange}
                    value={formik.values.username}
                    InputProps={{
                      readOnly: editMode,
                    }}
                    error={
                      formik.touched.username && Boolean(formik.errors.username)
                    }
                  />
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={3}>
                <FormGroup>
                  <TextField
                    label="password"
                    id="password"
                    type="password"
                    onChange={formik.handleChange}
                    value={formik.values.password}
                    InputProps={{
                      readOnly: editMode,
                    }}
                    error={
                      formik.touched.password && Boolean(formik.errors.password)
                    }
                  />
                </FormGroup>
              </Grid>
             

             
              <Grid item xs={6} md={3}>
                <FormGroup>
                  <TextField
                    label="port"
                    id="port"
                    onChange={formik.handleChange}
                    value={formik.values.port}
                    InputProps={{
                      readOnly: editMode,
                    }}
                    error={
                      formik.touched.port && Boolean(formik.errors.port)
                    }
                  />
                </FormGroup>
              </Grid>
            </Grid>

            <div className={classes.containerButton}>
              <Button
                size="large"
                className={classes.buttonTest}
                variant="contained"
                onClick={testDataSourceHandler}
                error={
                  formik.touched.sourceName && Boolean(formik.errors.sourceName)
                }
              >
                Test
              </Button>
              <div className={classes.containerButtonRight}>
                {renderSaveEditButton()}
                <Button
                  size="large"
                  startIcon={<DeleteOutlineIcon />}
                  color="error"
                  onClick={deleteDataSourceHandler}
                  variant="contained"
                >
                  Trash
                </Button>
              </div>
            </div>
          </form>
        </AccordionDetails>
      </Accordion>
    </React.Fragment>
  );
});

export default InfoDataSource;
