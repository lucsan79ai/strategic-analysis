import * as React from "react";

import {
  Typography,
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup
} from "@mui/material";

import {entityType, originType} from '../../../config/constants'


const NewEntityQuery = (props) => {
  const [entityTypeSelection, setEntitySelection] = React.useState(props.currentEntityType)
  const [originTypeSelection, setOriginSelection] = React.useState(props.currentOriginType)

  const updateEntitySelection = (event) => {
      // event.persist();
      const name = event.target.value;
      props.handleCurrentEntityType(name)
      setEntitySelection(name);
  };

  const updateOriginSelection = (event) => {
      // event.persist();
      const name = event.target.value;
      props.handleCurrentOriginType(name);
      setOriginSelection(name);
  };


if (props.visible == true) 
   return (
      <>
          <Typography variant="h6" color="primary">
              Step One
          </Typography>

          <Typography variant="overline" display="block" gutterBottom>
              1) Select Entity Type
          </Typography>

          <div style={{paddingLeft: '30px', paddingBottom: '30px'}}>
              <FormControl>
                  <RadioGroup
                      aria-labelledby="demo-radio-buttons-group-label"
                      defaultValue={entityType.PRODUCT}
                      name="radio-buttons-group"
                      onChange={updateEntitySelection}
                  >
                      <FormControlLabel value= {entityType.PRODUCT} control={<Radio />}  label={entityType.PRODUCT} checked={entityTypeSelection==entityType.PRODUCT}/>
                      <FormControlLabel value={entityType.BOM} control={<Radio />} label={entityType.BOM} checked={entityTypeSelection==entityType.BOM} />
                      <FormControlLabel value={entityType.CHANGE} control={<Radio />} label={entityType.CHANGE} checked={entityTypeSelection==entityType.CHANGE}/>
                      <FormControlLabel value={entityType.WHERE_USED} control={<Radio />}  label={entityType.WHERE_USED} checked={entityTypeSelection==entityType.WHERE_USED}/>
                      
                  </RadioGroup>
              </FormControl>
          </div>      


          <Typography variant="overline" display="block" gutterBottom>
              2) Select Origin Type
          </Typography>

          <div style={{paddingLeft: '30px'}}>
              <FormControl>
                  <RadioGroup
                      aria-labelledby="demo-radio-buttons-group-label"
                      defaultValue={originType.TABLE}
                      name="radio-buttons-group"
                      onChange={updateOriginSelection}
                  >
                      <FormControlLabel value={originType.TABLE} control={<Radio />}  label={originType.TABLE} checked={originTypeSelection==originType.TABLE}/>
                      <FormControlLabel value={originType.QUERY} control={<Radio />} label={originType.QUERY} checked={originTypeSelection== originType.QUERY} />
                      
                  </RadioGroup>
              </FormControl>
          </div>      

                
      </>
    )
  else return (<></>)
}

export default NewEntityQuery