import React, {useEffect} from "react";
import { 
  CURRENT_ENTITY_BUSINESS_OBJECT
} from "../../../states/actions"
import { useContext } from "react";
import { AuthContext } from "../../../states/context.js";

import {
  Typography,
  Select,
  InputLabel,
  FormControl,
  MenuItem,
  Button,
  Grid,
  FormGroup,
  TextField,
} from "@mui/material";





const NewEntityPlmCadStep2 = (props) => {

  useEffect(() => {
    'useEffect NewEntityPlmCadStep2 called'
    props.setCanGoOn(false);
  }, []);


  const ctx=useContext(AuthContext)
  const {entityDispatch, entityContext} = ctx




  const handleFieldsUpdate = (fieldName, e) => {
      console.log('Field: ' + fieldName + ' has changed: ' + e.target.value)
      entityDispatch({type: CURRENT_ENTITY_BUSINESS_OBJECT, payload: e.target.value})
      props.setCanGoOn(true);


  } 


  if (props.visible == true) 
    return (
      <React.Fragment>

          <Typography variant="h6" color="primary">
            Step two
          </Typography>

          <Typography variant="overline" display="block" gutterBottom>
            Please, select the business object
          </Typography>

        <Grid>

          <Grid item xs={12} md={12}>

              <InputLabel>Business object</InputLabel>
              <FormGroup className="formGroup">
          
                <Select
                  labelId="originName"
                  id="originName"
                  type="text"
                  value={entityContext.currentEntity.businessObject}
                  label="Origin Name"
                  onChange = { e => handleFieldsUpdate ("originName", e)}
                  // error={
                  //   formik.touched.originName && Boolean(formik.errors.originName)
                  // } 
                >
                  <MenuItem value={"Tabella1"}>Domain1</MenuItem>
                  <MenuItem value={"Tabella2"}>Domain2</MenuItem>
                  <MenuItem value={"Tabella3"}>Domain3</MenuItem>
                  <MenuItem value={"..."}>...</MenuItem>
                </Select>
                </FormGroup>
            </Grid>

          

          





          </Grid>


      </React.Fragment>
    )
    else return (<></>)

}

export default NewEntityPlmCadStep2