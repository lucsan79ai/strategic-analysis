import React, {useEffect} from "react";
import { 
  CURRENT_ENTITY_DOMAIN
} from "../../../states/actions"
import { useContext } from "react";
import { AuthContext } from "../../../states/context.js";

import {
  Typography,
  Select,
  InputLabel,
  FormControl,
  MenuItem,
  Button,
  Grid,
  FormGroup,
  TextField,
} from "@mui/material";





const NewEntityPlmCadStep1 = (props) => {

  useEffect(() => {
    props.setCanGoOn(false);
  }, []);

  const ctx=useContext(AuthContext)
  const {entityDispatch, entityContext} = ctx




  const handleChange = (fieldName, e) => {
      console.log('Field: ' + fieldName + ' has changed: ' + e.target.value)
      entityDispatch({type: CURRENT_ENTITY_DOMAIN, payload: e.target.value})
      props.setCanGoOn(true);
  } 


  if (props.visible == true) 
    return (
      <React.Fragment>

          <Typography variant="h6" color="primary">
            Step one
          </Typography>

          <Typography variant="overline" display="block" gutterBottom>
            Please, select the domain
          </Typography>

        <Grid>

          <Grid item xs={12} md={12}>

              <InputLabel>Domain</InputLabel>
              <FormGroup className="formGroup">
          
                <Select
                  labelId="originName"
                  id="originName"
                  type="text"
                  value={entityContext.currentEntity.domain}
                  label="Origin Name"
                  onChange = { e => handleChange ("originName", e)}
                  // error={
                  //   formik.touched.originName && Boolean(formik.errors.originName)
                  // } 
                >
                  <MenuItem value={"Domain1"}>Domain1</MenuItem>
                  <MenuItem value={"Domain2"}>Domain2</MenuItem>
                  <MenuItem value={"Domain3"}>Domain3</MenuItem>
                  <MenuItem value={"..."}>...</MenuItem>
                </Select>
                </FormGroup>
            </Grid>

          

          





          </Grid>


      </React.Fragment>
    )
    else return (<></>)

}

export default NewEntityPlmCadStep1