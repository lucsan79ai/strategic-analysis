import React, { useEffect } from "react";
import TableEntitySchema from '../../entityDetail/TableEntitySchema/TableEntitySchema'

import { 
  CURRENT_ENTITY_SCHEMA_ARRAY
} from "../../../states/actions"

import { useContext } from "react";
import { AuthContext } from "../../../states/context.js";


import {
  Typography,
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup
} from "@mui/material";

import TagComponent from '../../entityDetail/TagComponent/TagComponent'

import { getEntitySchemaFromNewSources } from '../../../services/backend/dataSourcesBackend'


  const NewEntityQuery = (props) => {

    useEffect( async () => {
      const entityData = await getEntitySchemaFromNewSources({}) 
      entityDispatch({type: CURRENT_ENTITY_SCHEMA_ARRAY, payload: entityData})    
    }, []);


    //todo
    const handleDropdownChange = (param,item) => {
      console.log('dropdown has changed: ', param)
      console.log('dropdown has changed: ', item)
    }

    const ctx=useContext(AuthContext)
    const {entityDispatch, entityContext} = ctx

    const data  = entityContext.currentEntity.currentSchemaArray

    if (props.visible && data) 
    return (
      <>
          <Typography variant="h6" color="primary">
              Step three
          </Typography>

          <Typography variant="overline" display="block" gutterBottom>
              Schema Optimization
          </Typography>

          <TableEntitySchema entityData={ data } colorHeader="#90A1AD" handleDropdownChange={handleDropdownChange}/>
          
      </>
    )
  else return (<></>)
}

export default NewEntityQuery