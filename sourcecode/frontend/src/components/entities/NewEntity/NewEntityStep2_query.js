import * as React from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { 
    CURRENT_ENTITY, CURRENT_ENTITY_INIT, CURRENT_ENTITY_SCHEMA_ARRAY
} from "../../../states/actions"
import { useContext, useState } from "react";
import { AuthContext } from "../../../states/context.js";
import { format } from 'sql-formatter';

import {
    Typography,
    Select,
    InputLabel,
    FormControl,
    MenuItem,
    Button,
    Grid,
    FormGroup,
    TextField,
    TextareaAutosize,
    Alert, Stack
} from "@mui/material";

import { getEntitySchemaFromNewSources } from '../../../services/backend/dataSourcesBackend'
import { height } from "@mui/system";




const NewEntityQuery = (props) => {

  const ctx = useContext(AuthContext)
  const { entityDispatch, entityContext } = ctx



  const submitHandler = async (event, values) => {
    console.log("submit");
    
    entityDispatch({type: CURRENT_ENTITY, payload: formik.values})

    // const entityData = await getEntitySchemaFromNewSources({}) 
    // entityDispatch({type: CURRENT_ENTITY_SCHEMA_ARRAY, payload: entityData})


    console.log('entityContext: ', JSON.stringify(entityContext))


    //DEVE ANDARE DI SOTTO ALTRIMENTI IL CONTEXT NON AGGIORNA LO STATO
    props.setCanGoOn(true);

  }

  const initialValues = {
    entityType: props.currentEntityType,
    originType: props.currentOriginType,
    label: entityContext.currentEntity.label,
    description: entityContext.currentEntity.description,
    originDirectQuery: entityContext.currentEntity.originDirectQuery,
  };


  const validationSchema = Yup.object({
    entityType: Yup.string().required(),
    label: Yup.string().required(),
    description: Yup.string(),
    originType: Yup.string().required(),
    originDirectQuery: Yup.string().required(),
  });


  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: validationSchema,
    onSubmit: submitHandler
  });


  const handleCleanData = (e) => {

    // formik.setFieldValue("entityType", "" )
    formik.setFieldValue("label", "" )
    formik.setFieldValue("description", "" )
    formik.setFieldValue("originType", "" )
    formik.setFieldValue("originName", "" )
    formik.setFieldValue("originDirectQuery", "" )

    entityDispatch({type: CURRENT_ENTITY_INIT})

    props.setCanGoOn(false);

  }

  const handleFieldsUpdate = (fieldName, e) => {
    console.log('Field: ' + fieldName + ' has changed: ' + e.target.value)
    formik.handleChange(e);
    formik.setFieldValue(fieldName, e.target.value )
  } 


  const renderAlertTextarea = () => {
      if (formik.errors.originDirectQuery) 
        return (
          <Stack sx={{ width: '100%' }} spacing={2}>
            <Alert severity="error">Error in query!</Alert>
          </Stack>
        )

      else 
        return (<></>)
  }





   return (
    <React.Fragment>
      <form onSubmit={formik.handleSubmit}>      

        <Typography variant="h6" color="primary">
          Step Two
        </Typography>

        <Typography variant="overline" display="block" gutterBottom>
          Fill information for QUERY origin type
        </Typography>

        <Button onClick={handleCleanData} style={{padding: '20px'}}>Clean data</Button>

        <Grid
          container
          spacing={5}
          display="flex"
          alignItems={"center"}
          justifyContent="space-between"
        >


        <Grid item xs={12} md={6}>
            <FormGroup className="formGroup">
                <TextField
                    id="entityType"
                    type="text"
                    label="Entity Type"
                    value={formik.values.entityType}
                    variant="standard"

                />
            </FormGroup>
        </Grid>


        <Grid item xs={12} md={6}>
            <FormGroup className="formGroup">
                <TextField
                    id="originType"
                    type="text"
                    label="Origin Type"
                    value={formik.values.originType}
                    variant="standard"

                />
            </FormGroup>
        </Grid>


        <Grid item xs={12} md={6}>
            <FormGroup className="formGroup">
                <TextField
                    id="label"
                    type="text"
                    label="label"
                    onChange = { e => handleFieldsUpdate ("label", e)}
                    value={formik.values.label}
                    error={
                      formik.touched.label && Boolean(formik.errors.label)
                    }
                />
            </FormGroup>
        </Grid>


        <Grid item xs={12} md={6}>
            <FormGroup className="formGroup">
                <TextField
                    id="description"
                    type="text"
                    label="description"
                    onChange = { e => handleFieldsUpdate ("description", e)}
                    value={formik.values.description}
                    error={
                      formik.touched.description && Boolean(formik.errors.description)
                    }
                />
            </FormGroup>
        </Grid>

       
        
         <Grid item xs={12} md={12}>
            <FormGroup className="formGroup">
              <TextareaAutosize
                id="originDirectQuery"

                maxRows={5}
                aria-label="maximum height"
                placeholder="Copy and paste query"
                // defaultValue="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                //     ut labore et dolore magna aliqua."
                onChange = { e => handleFieldsUpdate ("originDirectQuery", e)}
                error={
                  formik.touched.originDirectQuery && Boolean(formik.errors.originDirectQuery)
                }

                value={format(formik.values.originDirectQuery)}
                style={{ height: 150, fullWidth: '100%' }}
              />
                      {renderAlertTextarea()}

            </FormGroup>
        </Grid> 

 
        {/* <Grid item xs={12} md={12}>
            <FormGroup className="formGroup">
                <TextField
                    id="originDirectQuery"
                    type="text"
                    label="Query"
                    onChange = { e => handleFieldsUpdate ("originDirectQuery", e)}
                    value={formik.values.originDirectQuery}
                    error={
                      formik.touched.originDirectQuery && Boolean(formik.errors.originDirectQuery)
                    }
                    inputProps= {{
                      style: {
                        height: '150px'                      },
                    }}
                                    />
            </FormGroup>
        </Grid>  */}



        <Grid item xs={12} md={12} >
            <div style={{margin: '10px'}}>
            <Button
              fullWidth
              type="submit"
              variant="outlined"
              color="secondary"
              size="large"
              onSubmit={formik.onSubmit}>
            
              Validate data and go on
            </Button>
            </div>
          </Grid>

        </Grid>


      </form>
    </React.Fragment>
  )
}

export default NewEntityQuery