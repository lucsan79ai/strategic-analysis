import * as React from "react";

import {
  Typography,
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup
} from "@mui/material";

import TagComponent from '../../entityDetail/TagComponent/TagComponent'

const NewEntityQuery = (props) => {
  const mockData = {
    id: -1
  }

  const handleDelete = (value) => {
    console.log('handleDelete')

  }

  const handleSubmit = (value) => {
    console.log('handleDelete')

  }


  if (props.visible == true) 
  return (
     <>
        <Typography variant="h6" color="primary">
            Step Four
        </Typography>

        <Typography variant="overline" display="block" gutterBottom>
            manage entity topics
        </Typography>

        <TagComponent entityData = {mockData}  isNew = {true} handleDelete={handleDelete} handleSubmit={handleSubmit}/>
     </>
   )
 else return (<></>)
}

export default NewEntityQuery