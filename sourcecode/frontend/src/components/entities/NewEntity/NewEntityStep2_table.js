import * as React from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { 
  CURRENT_ENTITY, CURRENT_ENTITY_INIT, CURRENT_ENTITY_SCHEMA_ARRAY
} from "../../../states/actions"
import { useContext } from "react";
import { AuthContext } from "../../../states/context.js";

import {
  Typography,
  Select,
  InputLabel,
  FormControl,
  MenuItem,
  Button,
  Grid,
  FormGroup,
  TextField,
} from "@mui/material";

import { getEntitySchemaFromNewSources } from '../../../services/backend/dataSourcesBackend'




const NewEntityTable = (props) => {

  const ctx=useContext(AuthContext)
  const {entityDispatch, entityContext} = ctx

  const submitHandler = async (event, values) => {
    console.log("submit");

/*     props.setCanGoOn(true);
 */
    const entityData = await getEntitySchemaFromNewSources({}) 

    console.log("ENTITY DATA ------> ")
    console.log(entityData)


    
    entityDispatch({type: CURRENT_ENTITY, payload: formik.values})
    entityDispatch({type: CURRENT_ENTITY_SCHEMA_ARRAY, payload: entityData})

    
    console.log(entityData)

    console.log('entityContext: ', JSON.stringify(entityContext))

    //DEVE ANDARE DI SOTTO ALTRIMENTI IL CONTEXT NON AGGIORNA LO STATO
    props.setCanGoOn(true);

  }

  const initialValues = {
    entityType: props.currentEntityType,
    originType: props.currentOriginType,

    label: entityContext.currentEntity.label,
    description: entityContext.currentEntity.description,
    originName: entityContext.currentEntity.originName
    // originDirectQuery: entityContext.currentEntity.originDirectQuery || "",
    // originNavigationUrl: entityContext.currentEntity.originNavigationUrl || "",
    // currentTags: [],
    // currentSchema:[]
  };


  const validationSchema = Yup.object({

    entityType: Yup.string().required(),
    label: Yup.string().required(),
    description: Yup.string(),
    originType: Yup.string().required(),
    originName: Yup.string().required(),
    // originDirectQuery: Yup.string().required(),
    // originNavigationUrl: Yup.string().required()

  });


  const formik = useFormik({
      initialValues: initialValues,
      validationSchema: validationSchema,
      onSubmit: submitHandler
  });


  const handleCleanData = (e) => {

      // formik.setFieldValue("entityType", "" )
      formik.setFieldValue("label", "" )
      formik.setFieldValue("description", "" )
      formik.setFieldValue("originType", "" )
      formik.setFieldValue("originName", "" )
      // formik.setFieldValue("originDirectQuery", "" )
      // formik.setFieldValue("originNavigationUrl", "" )

      entityDispatch({type: CURRENT_ENTITY_INIT})

      props.setCanGoOn(false);

  }


  const handleFieldsUpdate = (fieldName, e) => {
      console.log('Field: ' + fieldName + ' has changed: ' + e.target.value)
      formik.handleChange(e);
      formik.setFieldValue(fieldName, e.target.value )
  } 



   return (
    <React.Fragment>
      <form onSubmit={formik.handleSubmit}>      

        <Typography variant="h6" color="primary">
          Step Two
        </Typography>

        <Typography variant="overline" display="block" gutterBottom>
          Fill information for TABLE origin type
        </Typography>

        <Button onClick={handleCleanData} style={{padding: '20px'}}>Clean data</Button>

        <Grid
          container
          spacing={5}
          display="flex"
          alignItems={"center"}
          justifyContent="space-between"
        >


        <Grid item xs={12} md={6}>
            <FormGroup className="formGroup">
                <TextField
                    id="entityType"
                    type="text"
                    label="Entity Type"
                    value={formik.values.entityType}
                    variant="standard"

                />
            </FormGroup>
        </Grid>


        <Grid item xs={12} md={6}>
            <FormGroup className="formGroup">
                <TextField
                    id="originType"
                    type="text"
                    label="Origin Type"
                    value={formik.values.originType}
                    variant="standard"

                />
            </FormGroup>
        </Grid>


        <Grid item xs={12} md={6}>
            <FormGroup className="formGroup">
                <TextField
                    id="label"
                    type="text"
                    label="label"
                    onChange = { e => handleFieldsUpdate ("label", e)}
                    value={formik.values.label}
                    error={
                      formik.touched.label && Boolean(formik.errors.label)
                    }
                />
            </FormGroup>
        </Grid>


        <Grid item xs={12} md={6}>
            <FormGroup className="formGroup">
                <TextField
                    id="description"
                    type="text"
                    label="description"
                    onChange = { e => handleFieldsUpdate ("description", e)}
                    value={formik.values.description}
                    error={
                      formik.touched.description && Boolean(formik.errors.description)
                    }
                />
            </FormGroup>
        </Grid>

        <Grid item xs={12} md={12}>

            <InputLabel>Origin Name</InputLabel>
            <FormGroup className="formGroup">
        
              <Select
                labelId="originName"
                id="originName"
                type="text"
                value={formik.values.originName}
                label="Origin Name"
                onChange = { e => handleFieldsUpdate ("originName", e)}
                error={
                  formik.touched.originName && Boolean(formik.errors.originName)
                } 
              >
                <MenuItem value={"Tabella1"}>Tabella1</MenuItem>
                <MenuItem value={"Tabella2"}>Tabella2</MenuItem>
                <MenuItem value={"Tabella3"}>Tabella3</MenuItem>
                <MenuItem value={"..."}>...</MenuItem>
              </Select>
              </FormGroup>
          </Grid>

        
        {/*         
          <Grid item xs={12} md={12}>
            <FormGroup className="formGroup">
                <TextField
                    id="originName"
                    type="text"
                    label="Origin Name"
                    onChange={formik.handleChange}
                    value={formik.values.originName}
                    error={
                      formik.touched.originName && Boolean(formik.errors.originName)
                    }
                />
            </FormGroup>
        </Grid> 
        */}

        



        <Grid item xs={12} md={12} >
            <div style={{margin: '10px'}}>
            <Button
              fullWidth
              type="submit"
              variant="outlined"
              color="secondary"
              size="large"
              onSubmit={formik.onSubmit}>
            
              Validate data and go on
            </Button>
            </div>
          </Grid>

        </Grid>


      </form>
    </React.Fragment>
  )
}

export default NewEntityTable