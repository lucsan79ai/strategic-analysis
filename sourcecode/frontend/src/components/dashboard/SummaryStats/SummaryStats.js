import React from "react";
import { Paper, Typography } from "@mui/material";
import useStyles from "./index";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";
import TimelineTwoToneIcon from "@mui/icons-material/TimelineTwoTone";

const SummarStats = ({ titleSummary, value, percentage }) => {
  const classes = useStyles();

  return (
    <Paper className={classes.widjetContainer}>
      <div className={classes.firstSection}>
        <Typography variant="subtitle1" className={classes.textResponsive}>
          {titleSummary}
        </Typography>
        <Typography
          variant="h6"
          className={classes.textResponsive}
          fontWeight={600}
        >
          {value}
        </Typography>
        <div className={classes.percentageSection}>
          <Typography className={classes.textResponsive} variant="subtitle1">
            {percentage > 0 ? (
              <div className={classes.percentageSubsection}>
                <ArrowUpwardIcon color="success" />
                <Typography variant="subtitle1">+{`${percentage}%`}</Typography>
              </div>
            ) : (
              <div className={classes.percentageSubsection}>
                {" "}
                <ArrowDownwardIcon color="error" />
                <Typography variant="subtitle1">{`${percentage}%`}</Typography>
              </div>
            )}
          </Typography>
          <Typography
            className={classes.textResponsive}
            variant="subtitle1"
            ml={1}
          >
            Last Week
          </Typography>
        </div>
      </div>

      <div className={classes.secondSection}>
        <TimelineTwoToneIcon color="primary" className={classes.svg_icons} />
      </div>
    </Paper>
  );
};

export default SummarStats;
