import { makeStyles } from "@mui/styles";

export default makeStyles((theme)=>({

    widjetContainer:{
        padding:'15px',
        display:'flex',
        justifyContent:'space-between',
        alignItems:'center'
    },

    firstSection:{
        display:'flex',
        flexDirection:'column',
        marginLeft:'4px',
    },
    secondSection:{

    },
    svg_icons:{
        fontSize:40
      },
    percentageSection:{
        display:"flex",
        alignItems:'center',
        justifyContent:'center',
    },
    percentageSubsection:{
        display:"flex",
        alignItems:'center',
        justifyContent:'center',
    },
    textResponsive:{
        [theme.breakpoints.down("md")]:{
            fontSize:'1.0rem',
    
    }
    }
}))