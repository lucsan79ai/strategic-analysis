import { makeStyles } from "@mui/styles";

export default makeStyles(()=>({
    paper: {
        padding: "12px",
        maxHeight: "403px",
        height: "403px",
      },
      title: {
        borderBottom: "3px solid #e3dede",
      },
      graph: {
        marginTop: "10px",
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: "20px",
        borderBottom: "3px solid #e3dede",

      },
      bottomContainer: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: "20px",
      },
      dataContainer:{
        display:"flex",
        alignItems:"center"
      },
      totalContainer:{
        display:"flex",
        justifyContent:"center",
        flexDirection:"column"
      },

      flexColumnStart:{
        display:"flex",
        flexDirection:"column",
        justifyContent:"start"
      }
    


}))