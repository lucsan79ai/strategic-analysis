import React from "react";
import BarChart from "./BarChart/BarChart";
import useStyles from "./index";
import { Paper, Typography } from "@mui/material";

const Revenue = () => {
  const classes = useStyles();

  return (
    <Paper className={classes.paper}>
      <div className={classes.title}>
        <Typography variant="subtitle1">Revenue</Typography>{" "}
      </div>
      <div className={classes.graph}>
        <div className={classes.totalContainer}>
          <Typography variant="subtitle1" color="rgb(181,188,212)">
            Total
          </Typography>
          <Typography variant="subtitle1" fontWeight={600}>
            108
          </Typography>
          <Typography variant="subtitle1" color="rgb(181,188,212)">
            +37,37%
          </Typography>
        </div>
        <BarChart />
      </div>

      <div className={classes.bottomContainer}>
        <div className={classes.flexColumnStart}>
          <div className={classes.dataContainer}>
            <Typography variant="subtitle1" fontWeight={600} mr={2}>
              108{" "}
            </Typography>
            <Typography variant="subtitle1" color="rgb(181,188,212)">
              +37,7%
            </Typography>
          </div>

          <Typography variant="subtitle1" color="rgb(181,188,212)">
            Cost
          </Typography>
        </div>

        <div className={classes.flexColumnStart}>
          <div className={classes.dataContainer}>
            <Typography variant="subtitle1" fontWeight={600} mr={2}>
              1168
            </Typography>
            <Typography variant="subtitle1" color="rgb(181,188,212)">
              -18.9%
            </Typography>
          </div>

          <Typography variant="subtitle1" color="rgb(181,188,212)">
            Revenue
          </Typography>
        </div>
      </div>
    </Paper>
  );
};

export default Revenue;
