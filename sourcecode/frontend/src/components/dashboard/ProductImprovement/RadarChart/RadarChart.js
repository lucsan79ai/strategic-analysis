import React from "react";
import {
  Chart as ChartJS,
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend,
} from "chart.js";
import { Radar } from "react-chartjs-2";

ChartJS.register(
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend
);

export const options = {
  plugins: {
    legend: false,
  },
};

const RadarChart = ({ dataRadar }) => {
  return (
    <Radar data={dataRadar} options={options} style={{ maxHeight: "200px" }} />
  );
};

export default RadarChart;
