import { Paper, Typography } from "@mui/material";
import { Grid } from "@mui/material";
import React from "react";
import useStyles from "./index";
import RadarChart from "./RadarChart/RadarChart";
import LinearProgress from "@mui/material/LinearProgress";

const ProductImprovemnt = ({ dataFirstCard }) => {
  const classes = useStyles();

  return (
    <Paper className={classes.paper}>
      <div className={classes.title}>
        <Typography variant="subtitle1">Product Improvement</Typography>
      </div>

      <div className={classes.graph}>
        <RadarChart dataRadar={dataFirstCard[0]} />
      </div>
      <Grid container spacing={2}>
        <Grid item xs={6} md={6}>
          <div className={classes.flexContainer}>
            <Typography variant="subtitle1" fontSize="12px">
              3D CAD
            </Typography>
            <Typography variant="subtitle1" fontSize="12px">
              186
            </Typography>
          </div>
          <div className={classes.flexContainer}>
            <LinearProgress
              variant="determinate"
              value={18}
              style={{ width: "70%" }}
            />
            <Typography variant="subtitle1" fontSize="12px">
              +18%
            </Typography>
          </div>
        </Grid>
        <Grid item xs={6} md={6}>
          <div className={classes.flexContainer}>
            <Typography variant="subtitle1" fontSize="12px">
              Reusable
            </Typography>
            <Typography variant="subtitle1" fontSize="12px">
              1008
            </Typography>
          </div>
          <div className={classes.flexContainer}>
            <LinearProgress
              variant="determinate"
              value={36}
              style={{ width: "70%" }}
            />
            <Typography variant="subtitle1" fontSize="12px">
              +36%
            </Typography>
          </div>
        </Grid>

        <Grid item xs={6} md={6}>
          <div className={classes.flexContainer}>
            <Typography variant="subtitle1" fontSize="12px">
              Sustainable
            </Typography>
            <Typography variant="subtitle1" fontSize="12px">
              118
            </Typography>
          </div>
          <div className={classes.flexContainer}>
            <Typography variant="subtitle1" fontSize="12px">
              Design
            </Typography>
            <Typography variant="subtitle1" fontSize="12px">
              +10%
            </Typography>
          </div>
        </Grid>
        <Grid item xs={6} md={6}>
          <div className={classes.flexContainer}>
            <Typography variant="subtitle1" fontSize="12px">
              Configure To
            </Typography>
            <Typography variant="subtitle1" fontSize="12px">
              67
            </Typography>
          </div>
          <div className={classes.flexContainer}>
            <Typography variant="subtitle1" fontSize="12px">
              order
            </Typography>
            <Typography variant="subtitle1" fontSize="12px">
              +16%
            </Typography>
          </div>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default ProductImprovemnt;
