import React from "react";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Doughnut } from "react-chartjs-2";
import ChartDataLabels from "chartjs-plugin-datalabels";

ChartJS.register(ChartDataLabels,ArcElement, Tooltip, Legend);

export const options = {
  plugins: {
    legend: false,
    datalabels: {
      display: true,
      color: "white",
      formatter: (value, context) => {
        return `${value}%`;
      },
    },
  },
};


const DonutsChart = ({dataDonuts}) => {
  return (
    <Doughnut data={dataDonuts} options={options} style={{ maxHeight: "145px" }} />
  );
};

export default DonutsChart;
