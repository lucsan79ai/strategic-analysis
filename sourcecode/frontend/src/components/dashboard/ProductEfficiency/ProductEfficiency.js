import { Paper, Typography, Grid, LinearProgress } from "@mui/material";
import useStyles from "./index";
import React from "react";
import DonutsChart from "./DonutsChart/DonutsChart";

const ProductEfficiency = ({ dataSecondCard }) => {

  const classes = useStyles();

  return (
    <Paper className={classes.paper}>
      <div className={classes.title}>
        <Typography variant="subtitle1">Product Efficiency</Typography>
      </div>

      <div className={classes.graph}>
         <DonutsChart dataDonuts={dataSecondCard[0]} />
       </div>

      <Grid
        container
        spacing={2}
        alignItems={"center"}
        justifyContent={"space-between"}
      >
        <Grid item xs={12} md={12}>
          <div className={classes.flexContainer}>
            <div className={classes.containerColumn}>
              <Typography variant="subtitle1" fontSize="12px" fontWeight={600}>
                Design
              </Typography>
              <Typography
                variant="subtitle1"
                fontSize="11px"
                color="rgb(181,188,212)"
              >
                Global,Services
              </Typography>
            </div>
            <Typography
              variant="subtitle1"
              fontSize="12px"
              width={"20%"}
              fontWeight={600}
              ml={"auto"}
            >
              85%
            </Typography>

            <LinearProgress
              variant="determinate"
              value={85}
              style={{ width: "30%" }}
            />
          </div>
        </Grid>

        <Grid item xs={12} md={12}>
          <div className={classes.flexContainer}>
            <div className={classes.containerColumn}>
              <Typography
                variant="subtitle1"
                fontSize="12px"
                fontWeight={600}
                width={"100%"}
              >
                New Product Development
              </Typography>
              <Typography
                variant="subtitle1"
                fontSize="11px"
                color="rgb(181,188,212)"
              >
                Global,Services
              </Typography>
            </div>
            <Typography
              variant="subtitle1"
              fontSize="12px"
              width={"20%"}
              fontWeight={600}
              ml={"auto"}
            >
              75%
            </Typography>

            <LinearProgress
              variant="determinate"
              value={75}
              style={{ width: "30%" }}
            />
          </div>
        </Grid>

        <Grid item xs={12} md={12}>
          <div className={classes.flexContainer}>
            <div className={classes.containerColumn}>
              <Typography variant="subtitle1" fontSize="12px" fontWeight={600}>
                Product Maintanance
              </Typography>
              <Typography
                variant="subtitle1"
                fontSize="11px"
                color="rgb(181,188,212)"
              >
                Local,Dc
              </Typography>
            </div>
            <Typography
              variant="subtitle1"
              fontSize="12px"
              width={"20%"}
              fontWeight={600}
              ml={"auto"}
            >
              28%
            </Typography>

            <LinearProgress
              variant="determinate"
              value={28}
              style={{ width: "30%" }}
            />
          </div>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default ProductEfficiency;
