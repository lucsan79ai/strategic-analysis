import { makeStyles } from "@mui/styles";


export default makeStyles(() => ({
    paper: {
      padding: "12px",
      maxHeight:"403px",
      height:"403px",
      
    },
    title: {
      borderBottom: "3px solid #e3dede",
    },
    graph: {
      marginTop: "10px",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      padding: "20px",
    },
    flexContainer:{
      display:"flex",
      justifyContent:"space-between",
      alignItems:"center"
    },
    containerColumn:{
      display:"flex",
      justifyContent:"start",
      alignItems:"start",
      margin:0,
      flexDirection:"column"
    }
  }));
  