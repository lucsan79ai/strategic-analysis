import React from "react";

import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  pointDot: true,
  plugins: {
    legend: {
      position: "top",
    },
    datalabels: {
      display: false,
    },
  },

  scales: {
    y: {
      min: 0,
      max: 120,
      ticks: {
        // forces step size to be 50 units
        stepSize: 30,
      },
    },
  },
};

const LineChart = ({ dataGraph }) => {
  return (
    <Line options={options} data={dataGraph} style={{ maxHeight: "50vh" }} />
  );
};

export default LineChart;
