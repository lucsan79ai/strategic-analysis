import React, { Fragment, useState } from "react";
import ReactFlow, {
  removeElements,
  addEdge,
  MiniMap,
  Controls,
  Background,
} from "react-flow-renderer";
import { Paper,Button, TextField } from "@mui/material";
import ColorSelectorNode from './customNode';
import useStyles from './index'

//Nodi iniziali
const initialElements = [
  {
    id: "1",
    type: "input",
    data: {
      label: (
        <>
          <strong> Persona 1</strong>
        </>
      ),
      node: {
        name: "Ugo",
        surname: "Galliano",
      },
    },
    position: { x: 250, y: 0 },
  },
  {
    id: "2",
    data: {
      label: (
        <>
          This is a <strong>default node</strong>
        </>
      ),
      node: {
        name: "Ugo",
        surname: "Galliano",
      },
    },

    position: { x: 100, y: 100 },
  },
];

const onLoad = (reactFlowInstance) => {
  console.log("flow loaded:", reactFlowInstance);
  reactFlowInstance.fitView();
};

const ToolFlow = () => {

    //Elements are node and edges
  const [name, setName] = useState("");
  const [elements, setElements] = useState(initialElements);
  const classes=useStyles()
  

  const onElementsRemove = (elementsToRemove) => setElements((els) => removeElements(elementsToRemove, els));

  const onConnect = (params) => {
    console.log(params);
    setElements((els) => addEdge(params, els));
  };

  const addNode = () => {
    setElements((e) =>
      e.concat({
        id: (e.length + 1).toString(),
        data: { label: `${name}` },
        type:"selectorNode",
        position: {
          x:0,
          y: 0,
        },
      })
    );
  };



  return (
    <Fragment>
      <Paper>
        <ReactFlow
          elements={elements}
          onElementsRemove={onElementsRemove}
          style={{ width: "100%", height: "80vh" }}
          onConnect={onConnect}
          nodeTypes={{
            selectorNode: ColorSelectorNode,
          }}
          onElementClick={(e, element) => console.log(element)}
          onLoad={onLoad}
          snapToGrid={true}
          snapGrid={[15, 15]}
        >
          <MiniMap
            nodeStrokeColor={(n) => {
              if (n.style?.background) return n.style.background;
              if (n.type === "input") return "#0041d0";
              if (n.type === "output") return "#ff0072";
              if (n.type === "default") return "#1a192b";
              if (n.type === "selectorNode") return "#d02300"
              return "#eee";
            }}
            nodeColor={(n) => {
              if (n.style?.background) return n.style.background;
              if (n.type === "selectorNode") return "#d02300"

              return "#fff";
            }}
            
            nodeBorderRadius={2}
          />
          <Controls />
          <Background color="#aaa" gap={16} />
        </ReactFlow>
        <div className={classes.inputContainer}>
          <TextField
            type="text"
            onChange={(e) => setName(e.target.value)}
            name="title"
            margin={"normal"}
          />
          <Button type="button"  variant="contained" onClick={addNode}>
            Add Node
          </Button>
        </div>
      </Paper>
    </Fragment>
  );
};

export default ToolFlow;
