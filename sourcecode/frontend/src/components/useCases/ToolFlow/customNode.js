import React, { memo } from "react";

import { Handle } from "react-flow-renderer";


const customNodeStyles = {
    background: 'white',
    color: 'black',
    border:'1px solid red',
    padding: 10,
    borderRadius:'3px'
  };
  


export default memo(({ data, isConnectable }) => {
  return (
    <div style={customNodeStyles}>
      <Handle
        type="source"
        onConnect={(params) => console.log("handle onConnect", params)}
        isConnectable={isConnectable}
      />
      <div>{data.label}</div>
    </div>
  );
});
