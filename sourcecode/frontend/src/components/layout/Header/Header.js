import React, { useState } from "react";
import { AppBar, Toolbar, Avatar, IconButton } from "@mui/material";
import { Menu, PowerSettingsNew } from "@mui/icons-material";
import useStyles from "./index.js";
import useMediaQuery from "@mui/material/useMediaQuery";
import { Link } from "react-router-dom";

const Header = () => {
  const classes = useStyles();
  const [click, setClick] = useState(true);
  const matches = useMediaQuery("(max-width:900px)");

  const handleClick = () => (matches ? setClick(!click) : null);

  return (
    <React.Fragment>
      <AppBar position={"static"} color="inherit" className={classes.navbar}>
        <Toolbar className={classes.toolbar}>
          {matches ? (
            <div className={classes.hamburger}>
              <IconButton
                size="medium"
                sx={{ color: "white" }}
                onClick={handleClick}
              >
                <Menu />
              </IconButton>
            </div>
          ) : <div></div>}
          
          <div  className={click ? classes.menu : `${classes.menu} ${classes.active}`}>
            <Link to="/" onClick={handleClick} className={classes.menuStyles}>
              Home
            </Link>
            <Link
              to="/dataSources"
              onClick={handleClick}
              className={classes.menuStyles}
            >
              Data Sources
            </Link>
            <Link
              to="/use-cases"
              onClick={handleClick}
              className={classes.menuStyles}
            >
              Use Cases
            </Link>
          </div>

          <div className={classes.rightSection}>
            <IconButton sx={{ color: "white" }}>
              <PowerSettingsNew />
            </IconButton>
            <IconButton>
              <Avatar alt="Remy Sharp" />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
};

export default Header;
