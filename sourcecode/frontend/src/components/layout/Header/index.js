import { makeStyles } from "@mui/styles";

export default makeStyles((theme) => ({
  navbar: {
    top: "0",
    left: "0",
    position: "relative",
  },

  menu: {
    display: "flex",
    [theme.breakpoints.down("md")]: {
      position: "absolute",
      flexDirection: "column",
      alignItems: "center",
      top: "55px",
      right: "-100%",
      height: "20vh",
      width: "100%",
      background: theme.palette.secondary.main,
      zIndex: "10",
      transition: "0.4s ease-in-out",
    },
  },

  active: {
    right: 0,
  },

  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: theme.palette.secondary.main,
  },

  rightSection: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },

  menuStyles: {
    color: "white",
    fontWeight: 500,
    textDecoration: "none",
    marginRight: "1rem",
    [theme.breakpoints.down("md")]: {
      marginTop: "1rem",
    },
  },
}));
