# README #

Il collaudo o testing, appannaggio di uno o più tester, consiste nella verifica e validazione di quanto (misurabilità) il prodotto 
software implementato soddisfi i requisiti individuati dall'analisi. 
La relativa infrastruttura di supporto utilizzata in tale fase è detta ambiente di testing; 
il collaudo, in altre parole, valuta la correttezza rispetto alle specifiche. 
In caso di mancato rispetto delle specifiche il software, assieme al documento delle anomalie o bug, torna indietro agli sviluppatori 
con il compito di risolvere i problemi riscontrati attraverso anche il debugging del software. In genere le anomalie di funzionamento 
sono gestite tramite appositi software gestori di segnalazione anomalie, detti anche sistemi di ticketing o di bug tracking, che registrano 
i problemi segnalati al team di sviluppo e ne facilitano la relativa organizzazione, classificazione e gestione (p. es. Bugzilla, Mantis, Atlassian Jira, ecc.). 