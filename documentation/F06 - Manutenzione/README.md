# README #

La manutenzione comprende quelle sottoattività necessarie a modifiche del prodotto software successive alla distribuzione, al fine di correggere ulteriori errori 
attraverso patch (manutenzione correttiva), adattarlo a nuovi ambienti operativi (manutenzione adattativa o migrazione di ambiente), estenderne le funzionalità 
(manutenzione evolutiva) o convertirne l'implementazione in un'altra struttura (es. migrazione di framework o piattaforma). 