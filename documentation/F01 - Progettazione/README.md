# README #

Nell'attività di progettazione si definiscono le linee essenziali della struttura del prodotto software in funzione dei requisiti evidenziati dall'analisi, 
tipicamente appannaggio di un analista programmatore. 
Anche la progettazione può essere scomposta in sottoattività, dal progetto architetturale al progetto dettagliato. Si può dire che la progettazione ha lo 
scopo di definire (a un certo livello di dettaglio) la soluzione del problema. In questa fase sarà sviluppato un documento che permetterà di avere una definizione 
della struttura di massima (architettura di alto livello) e una definizione delle caratteristiche dei singoli componenti (moduli). 
