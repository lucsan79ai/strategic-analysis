# README #

Una volta che il software ha superato con successo le verifiche della fase di collaudo, esso viene pubblicato in una versione definitiva e messo a disposizione, 
secondo le regole della specifica licenza d'uso prescelta, di chiunque o dei soli acquirenti. 
La fase di installazione e messa in esercizio del software prodotto sulle piattaforme operative di utilizzo è detta deployment.
Talvolta si provvede anche alla messa in esercizio del software, cioè all'installazione e configurazione del prodotto software nell'infrastruttura di esecuzione 
utilizzabile dagli utenti, detta l'ambiente operativo o di produzione o di esercizio.  