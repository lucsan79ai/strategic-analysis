# README #

L'analisi è l'indagine preliminare del contesto in cui il prodotto software deve inserirsi, sulle caratteristiche o requisiti che deve esibire ed eventualmente su costi e 
aspetti logistici della sua realizzazione; questa fase può essere scomposta in sottoattività quali analisi di fattibilità, analisi e modellazione del dominio applicativo, 
analisi dei requisiti e così via. 
In senso più ampio si può dire che l'analisi ha lo scopo di definire (il più precisamente possibile) il problema da risolvere. 
Questa fase è costituita anche da raccolta dei dati tramite colloqui tra cliente/committente e relativi sviluppatori. 
Al termine della fase verrà creato un documento che descrive le caratteristiche del sistema, tale documento viene definito documento di specifiche funzionali. 
